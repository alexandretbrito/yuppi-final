(function(){
	'use strict';
	/**
	* patcher
	* conserta o caminho para os conteúdo na base de dados
	* Alexandre Brito 2017
	*/
	angular.module('APPlanner')
	.factory('patcher', patcher);

	function patcher($firebaseObject, $timeout, $rootScope, $firebaseArray, $location, $localStorage, accessFactory) {
		var patch = {
			trocaID: trocaID,// insere ID no evento e grava no local storage
			recID: recID,// grava o local storage no firebase
			reconTodo: reconTodo,// usa o calId antigo para traçar e mudar o ID do todo
			reconCheck: reconCheck,// usa o calId antigo para traçar e mudar o ID do checklist
			reconBudget: reconBudget,// usa o calId antigo para traçar e mudar o ID do orçamento
			reconCron: reconCron,// usa o calId antigo para traçar e mudar o ID do cronograma
			reconSuppliers: reconSuppliers,// usa o calId antigo para traçar e mudar o ID dos fornecedores
			reconShop: reconShop// usa o calId antigo para traçar e mudar o ID das compras
		};
		return patch;

		var list


		function trocaID(argument) {
			// body...
		};

		function recID(argument) {
			// body...
		};

		function reconTodo(argument) {
			// body...
		};

		function reconCheck(argument) {
			// body...
		};

		function reconBudget(argument) {
			// body...
		};

		function reconCron(argument) {
					// body...
		};

		function reconSuppliers(argument) {
			// body...
		};

		function reconShop(argument) {
			// body...
		};

	}// fim patcher - ATENÇÃO!!!
	})(); // fim do documento
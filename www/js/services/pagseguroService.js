(function() {
	'use strict';
angular
	.module('APPlanner')
	.factory('PagseguroService', PagseguroService);

	function PagseguroService($rootScope, $http, $q, $location, Utils, $localStorage, $sessionStorage, x2js, $window, $timeout){

	var sessao_01 = "";
	var PagSeguroDirectPayment;
	var cc = {
		'amex' : [34,37],
		'elo' : [636368, 438935,504175,451416,636297,5067,4576,4011],
		'visa' : [4],
		'master' : [51,52,53,54,55,677189],
		'diners' : [300,301,302,303,304,305,309,2014,2149,36,38,39],
		'hipercard' : [60],
		'aura' : [50],
		'discover' : [6011,622,64,65],
		'jcb' : [35]
	};

	if($window.PagSeguroDirectPayment){
		console.log("tem PagSeguroDirectPayment")
	}else{
		console.log("não tem PagSeguroDirectPayment")
	}

	// identificar sanbox
	var isSandbox = false;
	var headeds = 'application/json;charset=ISO-8859-1';
	var other_headeds = 'application/xml;charset=ISO-8859-1';
	var credencial = {};
	var vendaObj = {};
	var constante = {
			pagsegMail: 'thaismontechiari@gmail.com',
			pagsegToken: 'd9e76bf2-50cb-4297-b8a1-90f1600247bfbe0eea134533a0a2c29f5ae1eba417da9fbd-c334-4e4a-82d4-7ea857a41cfb'
	};
		/*
	if(isSandbox){

	  credencial.urlSession = "https://ws.sandbox.pagseguro.uol.com.br/v2/sessions?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
      credencial.urlPagSeguroDirectPayment = "https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
	  credencial.urlTransacao = 'https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals';
	  credencial.urlTransacao2 = 'https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals';	
    	credencial.urlSession = "/urlSession/v2/sessions?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
		credencial.urlPagSeguroDirectPayment = "/direct_pay/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
		credencial.urlTransacao = '/urlSession/pre-approvals';
		credencial.urlTransacao2 = '/urlSession/v2/pre-approvals';		
	
	}else{
		*/

      credencial.urlSession = "https://ws.pagseguro.uol.com.br/v2/sessions?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
      credencial.urlPagSeguroDirectPayment = "https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
      credencial.urlTransacao = 'https://ws.pagseguro.uol.com.br/pre-approvals';
	  credencial.urlTransacao2 = 'https://ws.pagseguro.uol.com.br/v2/pre-approvals';
	/* 
	  credencial.urlSession = "/urlSession/v2/sessions?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
	  credencial.urlPagSeguroDirectPayment = "/direct_pay/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
	  credencial.urlTransacao = '/urlSession/pre-approvals';
	  credencial.urlTransacao2 = '/urlSession/v2/pre-approvals';

	}
	 */


	var pagseg = {
		startSession: startSession,
		makeSession: makeSession,
		secureCard: secureCard,
		cardBrand: cardBrand,
		setPlan: setPlan,
		startSell: startSell,
		notifyMe: notifyMe,
		cancelPlan: cancelPlan,
		getTheBrand: getTheBrand
	}
	return pagseg

//--
	/* Coloca a sessão em Storage */
	function startSession(){
		var req = {
			method: 'POST',
			url: credencial.urlSession,
			headers:{
               'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
               'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
               'Content-Type': headeds,
			}
		};
		$http(req).then(function(response){
			console.log("sucesso!");
			var resp2 = x2js.xml_str2json(response.data);
			$sessionStorage.session_pag = resp2.session.id;
			console.log($sessionStorage.session_pag);
		}, function(error){
			console.log("erro na sessão")
			console.log(error)
		});
	}

 	function makeSession() {
 		$window.PagSeguroDirectPayment.setSessionId($sessionStorage.session_pag);
 		$timeout(function(){
 		 var desteLado = $window.PagSeguroDirectPayment.getSenderHash();
 		 console.log(desteLado)
 		}, 2000);
    };

    //pega o brando do cartão e envia para capturar o token
    function secureCard(card) {
			console.log("securecard")
		var cardao = card;
		var cardNumb = cardao.number
		cardNumb = cardNumb.substr(0, 6)
		var flag = getTheBrand(cardNumb);
		if (flag != undefined){
			cardBrand(cardao, flag);
		}else{
			$rootScope.$broadcast("failCardToken");
		};
    }

    //Capturar o token do cartão
    function cardBrand(cardNum, bandeira) {
			$window.PagSeguroDirectPayment.createCardToken({
	    		cardNumber: cardNum.number,
	    		brand: bandeira,
	    		cvv: cardNum.cvc,
	    		expirationMonth: cardNum.month,
	    		expirationYear: cardNum.year,
	    		success: function(response){
	    			console.log("sucesso no token")
	    			$sessionStorage.dados.cartao.token = response.card.token;
					console.log($sessionStorage.dados)
					$rootScope.$broadcast("successCardToken");
					setPlan();
	    		},
	    		error: function(error){
					console.log("erro no token")
	    			console.log(error);
					$rootScope.$broadcast("failCardToken");
	    		}
	    	})
    };

    //Cria o plano onflight
    function setPlan() {
    	var tipodepag = $sessionStorage.dados.preApproval;
					tipodepag.receiver = constante.pagsegMail;
					var itsos = '';
					itsos = "/request?email=" + constante.pagsegMail+
					"&token="+constante.pagsegToken+
					"&preApprovalName="+tipodepag.name+
					"&preApprovalCharge="+tipodepag.charge+
					"&preApprovalPeriod="+tipodepag.period+
					"&preApprovalAmountPerPayment="+tipodepag.amountPerPayment;
    	var envio_url = credencial.urlTransacao+itsos;
		var req3 = {
			method: 'POST',
			url: envio_url,
			headers:{
               'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
               'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
               'Accept': 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1',
               'Content-Type': headeds
			}
		};
		$http(req3).then(function(response){
			console.log ("procurando codes")
			console.log(response);
			$sessionStorage.dados.plano = response.data;
		}, function(error){
			console.log(error)
		});   	

    }

    function montaPlano(tipodepag){
			var pagDef = tipodepag;
			console.log("---")
    	console.log(pagDef)
    	var itsos = '';
			itsos = "/request?email=" + constante.pagsegMail+
			"&token="+constante.pagsegToken+
			"&preApprovalName="+pagDef.name+
			"&preApprovalCharge="+pagDef.charge+
			"&preApprovalPeriod="+pagDef.period+
			"&preApprovalAmountPerPayment="+pagDef.amountPerPayment;
    	return itsos;
    }

    function addYears(){
    	var d = new Date();
		var year = d.getFullYear()+1;
		var month = d.getMonth()+1;
		var day = d.getDate();
		if(day < 10){
			day = '0'+ day;
		}
		var c = day+'/'+month+'/'+year;
		return c;
    }
    // começa o processo de venda
    function startSell() {
    	Utils.show()
    	var desteLado = $window.PagSeguroDirectPayment.getSenderHash();
 		$timeout(function(){
 		//tratando o sender aqui
	    //CPF
	    vendaObj = {
     plan: $sessionStorage.dados.plano.code,
     reference: $localStorage.usuario.uid,
     sender:{
         name:$sessionStorage.dados.adesao.sender.name,
         email:$sessionStorage.dados.adesao.sender.email,
         ip:"192.168.0.1",
         hash:desteLado,
         phone :{
         areaCode:String($sessionStorage.dados.adesao.sender.phone.areaCode),
         number:String($sessionStorage.dados.adesao.sender.phone.number)
     },
     address:{
         street:$sessionStorage.dados.adesao.sender.address.street,
         number:$sessionStorage.dados.adesao.sender.address.number,
        complement:$sessionStorage.dados.adesao.sender.address.complement,
         district:$sessionStorage.dados.adesao.sender.address.district,
         city:$sessionStorage.dados.adesao.sender.address.city,
         state:$sessionStorage.dados.adesao.sender.address.state,
         country:"BRA",
         postalCode:$sessionStorage.dados.adesao.sender.address.postalCode
     },
     documents:[
         {
             type:"CPF",
             value:String($sessionStorage.dados.adesao.sender.documentacao.value)
         }
     ]
     },
     paymentMethod:{
         type:"CREDITCARD",
         creditCard:{
         token:$sessionStorage.dados.cartao.token,
         holder:{
             name:$sessionStorage.dados.cartao.nomecard,
             birthDate:$sessionStorage.dados.cartao.birthDate,
             documents:[
             {
                 type:"CPF",
                 value:String($sessionStorage.dados.cartao.cpf)
             }
         ],
         billingAddress:{
	         street:$sessionStorage.dados.adesao.sender.address.street,
	         number:$sessionStorage.dados.adesao.sender.address.number,
	         complement:$sessionStorage.dados.adesao.sender.address.complement,
	         district:$sessionStorage.dados.adesao.sender.address.district,
	         city:$sessionStorage.dados.adesao.sender.address.city,
	         state:$sessionStorage.dados.adesao.sender.address.state,
	         country:"BRA",
	         postalCode:$sessionStorage.dados.adesao.sender.address.postalCode
         },
         phone:{
	         areaCode:String($sessionStorage.dados.adesao.sender.phone.areaCode),
	         number:String($sessionStorage.dados.adesao.sender.phone.number)
         }
     }
     }
     }
 }

 		vendaObj = angular.toJson(vendaObj);
 		
    	console.log(vendaObj);

    	
    	var envio_url = credencial.urlTransacao+"?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;

		var req2 = {
			method: 'POST',
			url: envio_url,
			headers:{
               'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
               'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
               'Accept': 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1',
               'Content-Type': headeds
			},
			data: vendaObj
		};
		$http(req2).then(function(response){
			console.log("sucesso!");
			console.log(response);
			$sessionStorage.dados.aprovacao = response.data.code;
			$rootScope.$broadcast("successSellCode");
			Utils.hide();
		}, function(error){;
			console.log(error)
			Utils.hide();
			Utils.alertshow("Operação nao autorizada", "Tente novamente mais tarde.");
			$rootScope.$broadcast("failSellCode");
		});

	  }, 3000);
    };

    function notifyMe(){
    var estado = credencial.urlTransacao+"/"+$localStorage.usuario.preApproval+"?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
		var req4 = {
			method: 'GET',
			url: estado,
			headers:{
               'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
               'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
               'Accept': 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1',
			},
		};
		$http(req4).then(function(response){
		console.log("sucesso!");
			console.log(response);
			if(response.data.status == 'ACTIVE'){
				$rootScope.$broadcast("successStatus");
			}else{
				Utils.alertshow("Operação nao autorizada", "Contate sua operadora de cartão de crédito.");
			}
		}, function(error){;
			console.log(error)
			Utils.alertshow("Operação nao autorizada", "Tente novamente mais tarde.");
		});
    };

    function cancelPlan() {
			var oh_yes = false;
			var code = String($localStorage.usuario.preApproval)
			console.log(code)
			//var goodbye = credencial.urlTransacao2+"/cancel/"+$localStorage.usuario.preApproval+"?email=alexandre@sandbox.pagseguro.com.br&token=" + constante.pagsegToken;
			var goodbye = credencial.urlTransacao+"/"+$localStorage.usuario.preApproval+"/cancel/?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
			//var goodbye = "https://ws.sandbox.pagseguro.uol.com.br/pre-approvals/"+num+"/cancel/?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
			//var goodbye = credencial.urlTransacao/+num+"/cancel"
			//var goodbye = credencial.urlTransacao/+code+"/cancel/?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
			//--var goodbye = credencial.urlTransacao+"/"+$localStorage.usuario.preApproval+"/cancel?email=" + constante.pagsegMail + "&token=" + constante.pagsegToken;
			var req4 = {
			method: 'PUT',
			url: goodbye,
			headers:{
               'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
               'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
               'Accept': 'application/vnd.pagseguro.com.br.v3+xml;charset=ISO-8859-1',
               'Content-Type': other_headeds
			},
		};
		$http(req4).then(function(response){
		console.log("sucesso!");
		oh_yes = true;
			//var resultado = x2js.xml_str2json(response.data);
			//console.log(resultado);
			if(oh_yes){
				$rootScope.$broadcast("regretStatus");
			}else{
				Utils.alertshow("Houve um problema", "O cancelamento parece já ter ocorrido");
				$rootScope.$broadcast("cancelStatus");
			}
		}, function(error){;
			console.log(error)
			Utils.alertshow("Operação nao autorizada", "Tente novamente mais tarde.");
		});    	
    }

	function getTheBrand(corda){
		var numer = 0;
		var matched = false;
		var fio = '';
		var c = {
			'amex' : [34,37],
			'elo' : [636368, 438935,504175,451416,636297,5067,4576,4011],
			'visa' : [4],
			'mastercard' : [51,52,53,54,55,677189],
			'diners' : [300,301,302,303,304,305,309,2014,2149,36,38,39],
			'hipercard' : [60],
			'aura' : [50],
			'discover' : [6011,622,64,65],
			'jcb' : [35]
		};
		console.log(corda);
		console.log("///////////////////////")
		for (let ii = 0; ii < corda.length; ii++) {
			fio = corda.slice(0, numer);
			for(var a in c){
				if(!matched){
					for(var b in c[a]){
						if(fio == parseInt(c[a][b])){
							console.log("a bandeira é "+ a);
							matched = true;
							return a;
						}
					}
				}
			}
			numer++;
		}
	}

}; // fim da função
})();

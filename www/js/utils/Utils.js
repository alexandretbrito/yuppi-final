angular.module('APPlanner').factory('Utils', Utils)

	function Utils($ionicLoading,$ionicPopup) {

	var Utils = {
		show: show,
    spinnerCalendar: spinnerCalendar,
    spinnerEvents: spinnerEvents,
    spinnerStaff: spinnerStaff,
    spinnerBest: spinnerBest,
		hide: hide,
		alertshow: alertshow,
		errMessage: errMessage
	  };

	return Utils;

    function show() {
      $ionicLoading.show({
  	    animation: 'fadeIn',
  	    showBackdrop: true,
  	    maxWidth: 400,
  	    showDelay: 0,
        template: '<ion-spinner class="light"></ion-spinner><br />'
      });
    };

    function spinnerCalendar() {
      $ionicLoading.show({
        animation: 'fadeIn',
        showBackdrop: true,
        maxWidth: 400,
        showDelay: 0,
        template: '<ion-spinner></ion-spinner><br /><span>Aguarde, carregando calendário.</span>'
      });
    };

    function spinnerEvents() {
      $ionicLoading.show({
        animation: 'fadeIn',
        showBackdrop: true,
        maxWidth: 400,
        showDelay: 0,
        template: '<ion-spinner></ion-spinner><br /><span>Aguarde, carregando eventos.</span>'
      });
    };

     function spinnerStaff() {
      $ionicLoading.show({
        animation: 'fadeIn',
        showBackdrop: true,
        maxWidth: 400,
        showDelay: 0,
        template: '<ion-spinner></ion-spinner><br /><span>Aguarde! Estamos carregando sua lista de contatos.</span>'
      });
    };  


      function spinnerBest() {
      $ionicLoading.show({
        animation: 'fadeIn',
        showBackdrop: true,
        maxWidth: 400,
        showDelay: 0,
        template: '<ion-spinner></ion-spinner><br /><span>Aguarde! Estamos carregando sua lista de padrinhos.</span>'
      });
    };      

    function hide(){
      $ionicLoading.hide();
    };

	function alertshow(tit,msg){
		var alertPopup = $ionicPopup.alert({
			title: tit,
     cssClass: 'my-popup',   
			template: msg,
      okType: 'button-assertive myred', // String (default: 'button-positive'). The type of the OK button.
		});
	};




	function errMessage(err) {

    var msg = "Unknown Error...";

    if(err && err.code) {
      switch (err.code) {
        case "EMAIL_TAKEN":
           msg = "This Email has been taken."; break;
        case "INVALID_EMAIL":
          msg = "Invalid Email."; break;
         case "NETWORK_ERROR":
          msg = "Network Error."; break;
        case "INVALID_PASSWORD":
          msg = "Invalid Password."; break;
          msg = "Invalid User."; break;
      }
    }
		Utils.alertshow("Error",msg);
	};

};

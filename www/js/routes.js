angular.module('routes', [])
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
	.state('login', {
	    url: '/login',
	    templateUrl: 'templates/login.html',
	    controller: 'loginCtrl'
	})
  .state('forgot-password', {
      url: '/forgot-password',
      templateUrl: 'templates/forgot-password.html',
      controller: 'ForgotPasswordCtrl'
  })
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'PlannerCtrl'
  })
  .state('app.calendario', {
    url: '/calendario',
    views: {
      'menuContent': {
        templateUrl: 'templates/calendario.html',
        controller: 'CalendasCtrl'
      }
    }
  })
  .state('app.perfil', {
    url: '/perfil',
    views: {
      'menuContent': {
        templateUrl: 'templates/perfil.html',
        controller: 'PerfilCtrl'
      }
    }
  })
  .state('app.eventos', {
    url: '/eventos',
    views: {
      'menuContent': {
         cache: false,
        templateUrl: 'templates/eventos.html',
        controller: 'EvtsCtrl'
      }
    }
  })
  .state('app.eventounico', {
    url: '/eventounico',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/the_event.html',
        controller: 'TheEvtCtrl'
      }
    }
  })
  .state('app.novoevento', {
    url: '/novoevento',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/new_event.html',
        controller: 'NewEvtCtrl'
      }
    }
  })
  .state('app.editaevento', {
    url: '/editaevento',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/edit_event.html',
        controller: 'EditEvtCtrl'
      }
    }
  })
  .state('app.checklist', {
    url: '/checklist',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/checklist.html',
        controller: 'ChecklistCtrl'
      }
    }
  })
  .state('app.listadecompras', {
    url: '/listadecompras',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/shoplist.html',
        controller: 'ShopListCtrl'
      }
    }
  })
  .state('app.afazeres', {
    url: '/afazeres',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/afazeres.html',
        controller: 'ToDoListCtrl'
      }
    }
  })
  .state('app.cronograma', {
    url: '/cronograma',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/cronograma.html',
        controller: 'ChronoCtrl'
      }
    }
  })
  .state('app.cronogramacasorio', {
    url: '/cronogramacasorio',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/cronogramacasorio.html',
        controller: 'ChronoWedCtrl'
      }
    }
  })
  .state('app.cronogramainfantil', {
    url: '/cronogramainfantil',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/cronogramainfantil.html',
        controller: 'ChronoKidCtrl'
      }
    }
  })
  .state('app.receitas_despesas', {
    url: '/receitas_despesas',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/receitas_despesas.html',
        controller: 'BillsCtrl'
      }
    }
  })
  .state('app.grupo', {
    url: '/grupo',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/grupo.html',
        controller: 'GroupCtrl'
      }
    }
  })
  .state('app.fornecedores', {
    url: '/fornecedores',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/fornecedores.html',
        controller: 'SuppliersCtrl'
      }
    }
  })
   .state('app.galeria', {
    url: '/galeria',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/galeria.html',
        controller: 'GalleryCtrl'
      }
    }
  })
  .state('app.rsvp', {
    url: '/rsvp',
    views: {
      'menuContent': {
         cache: false,
        templateUrl: 'templates/rsvp.html',
        controller: 'ConvidadosCtrl'
      }
    }
  })
  .state('app.staff', {
    url: '/staff',
    views: {
      'menuContent': {
         cache: false,
        templateUrl: 'templates/staff.html',
        controller: 'StaffCtrl'
      }
    }
  })
  .state('app.logout', {
    url: '/logout',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/logout.html',
        controller: 'LogOutCtrl'
      }
    }
  })
  .state('app.reunioes', {
    url: '/reunioes',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/reunioes.html',
        controller: 'MeetingCtrl'
      }
    }
  })
  .state('app.visita-tecnica', {
    url: '/visita-tecnica',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/visita-tecnica.html',
        controller: 'visitaTecnicaCtrl'
      }
    }
  })
  // criação daa áreas de assinaturas //
  // ANDROID //
  .state('app.assinaturas', {
    url: '/ ',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/assinaturas.html',
        controller: 'assinaturasCtrl'
      }
    }
  })
    // IOS //
    .state('app.assinaturasIOS', {
      url: '/assinaturasIOS',
      views: {
        'menuContent': {
          cache: false,
          templateUrl: 'templates/assinaturasIOS.html',
          controller: 'assinaturasIOSCtrl'
        }
      }
    })
  .state('app.dados-cartao', {
    url: '/dados-cartao',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/dados-cartao.html',
        controller: 'credicardDataCtrl'
      }
    }
  })
  .state('app.resposta-assinatura', {
    url: '/resposta-assinatura',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/resposta-assinatura.html',
        controller: 'assinaretrieveCtrl'
      }
    }
  })
  .state('app.contato', {
    url: '/contato',
    views: {
      'menuContent': {
        cache: false,
        templateUrl: 'templates/contato.html',
        controller: 'canalCtrl'
      }
    }
  })
    // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});

(function() {
  'use_strict';

  angular.module('APPlanner').controller('assinaturasIOSCtrl', assinaturasIOSCtrl);

  function assinaturasIOSCtrl($scope, $rootScope, $localStorage, $filter, $sessionStorage, $ionicModal, $ionicPopover, $timeout, $location, Utils, $cordovaNetwork, planFactory, accessFactory) {


    $scope.options = {
      loop: false
    }

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      console.log("Slider initialized: ", data);
      $scope.slider = data.slider;
    });

    $scope.assinaturas = [];
    const IAP = window.inAppPurchase;
    $scope.pagante = $rootScope.usuarioAtivo.pagante;
    $scope.planos = ["br.com.brito.applanner.yuppi_mensal", "br.com.brito.applanner.yuppi_semestral", "br.com.brito.applanner.yuppi_anual"];
    $scope.badge = ["img/plano-bronze.png", "img/plano-prata.png", "img/plano-ouro.png"]
    $scope.description = ["Assinatura mensal do Yuppi, com acesso total e 15 dias de teste", "Assinatura semestral do Yuppi, com acesso total e 15 dias de teste", "Assinatura anual do app Yuppi, com todas as funções e 15 dias de teste"]
    document.addEventListener("deviceready", function () {
      $scope.connec = $cordovaNetwork.isOnline()
      console.log($scope.connec)
     });

     $scope.$on("$ionicView.enter", function(event, data){
       Utils.show()
       if(IAP){
        console.log("aqui estou em IOS") 
        IAP.getProducts($scope.planos).then(function(produtos){
          console.log("produtos aqui")
          Utils.hide()
          $scope.assinaturas = produtos;
          console.log($scope.assinaturas);
          console.log(`////////`)
          console.log(produtos);
          //$scope.apply()
        }).catch(function(error){
          console.log("erro")
          console.log(error)
        })
      };
     });

     $scope.buy = function(id_prod){
      IAP.buy(id_prod).then((data) => {
        console.log("comprado com sucesso")
        console.log(data);
        $localStorage.usuario.transactionId = data.transactionId;
        $localStorage.usuario.pagante = true;
        var novosDados2 = $localStorage.usuario;
        var dataChange2 = accessFactory.pegaUsuario($localStorage.usuario.uid);
        dataChange2.update(novosDados2).then(function(success){
            console.log("deu")
        }, function(error){
            console.log("não deu")
            console.log(error);
        })  
      })         
     }


  }; //fim da função
})(); // fim do documento

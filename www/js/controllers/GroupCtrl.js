(function() {
'use_strict';

angular.module('APPlanner').controller('GroupCtrl', GroupCtrl);

function GroupCtrl($scope, $rootScope, $location, ionicMaterialInk, $localStorage, $ionicPopup, $ionicModal, Utils, $timeout, planFactory, accessFactory){
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $scope.tuttiEventi = [];
    $scope.timePaster = false;

	$scope.$on("$ionicView.enter", function(event, data){

		if($localStorage.colabs){
	  			$rootScope.colabEvents = $localStorage.colabs;
	  			$scope.temEventos = true;
		  		evalEvents();
	  	}
	  	if(!$localStorage.colabs || $localStorage.colabs== {}){
	  		$scope.temEventos = false;
	  		retrieveColabs($localStorage.usuario.uid);
	  	}
  		showContent();
	});

	$scope.$on("$ionicView.leave", function(event, data){
			$scope.timePaster = false;
			console.log("the events are leaving the building...")		
	})		


function retrieveColabs(uid) {
	// procurar o uid nos grupos
	console.log("retrieveColabs")
	var mon_colabs = []
	var all_colabs = planFactory.allGroups();
	all_colabs.once("value", function(snapshot){
		var dicolabs = snapshot.val();
		console.log(dicolabs);
		angular.forEach(dicolabs, function(valor, chave){
		if(valor.colaboradores){
			console.log("tem colabs")
			angular.forEach(valor.colaboradores, function(valeus, key){
				if(valeus.uid == $localStorage.usuario.uid){
					console.log("estou aqui")
					var dadosColab = {};
					dadosColab.ID = valor.ID;
					dadosColab.owner = valor.owner;
					mon_colabs.push(dadosColab);
					console.log(mon_colabs);
				}
        	});
		}
		});	
		colabsParteDois(mon_colabs)
	});

}

function colabsParteDois(arra){

	var z= 0;
	var petardo = arra;
	console.log(petardo.length)
	angular.forEach(petardo, function(value, key){
			var evPath = accessFactory.pegaEventList(value.owner);
			var pathEv = evPath.child(value.ID)
			pathEv.once('value').then(function(snapshot){
				if(!$localStorage.colabs){
					$localStorage.colabs = {};
				}
				$localStorage.colabs[snapshot.val().ID] = snapshot.val();
				$localStorage.colabs[snapshot.val().ID].uid = value.owner;
				console.log($localStorage.colabs)
				$rootScope.colabEvents = $localStorage.colabs;
				z++;
				console.log(z)
				if(z == petardo.length){
					console.log("chegou aqui")
					console.log($rootScope.colabEvents)
					$scope.temEventos = true;
					evalEvents()
				}
			});
	});
}

function evalEvents() {
		var i=0
        var bjec = {};
        $scope.tuttiEventi = [];
        bjec = $rootScope.colabEvents;
        angular.forEach(bjec, function(shops, key){
            $scope.tuttiEventi[i] = angular.copy(shops);
            i++;
        });
        console.log("vai")
        console.log(bjec)
        console.log($scope.tuttiEventi);

}

	$scope.dia = function(evento){
		console.log("chegou o dia")
		var dengo = new Date(evento.evtDay);
		return dengo;
	}

	$scope.clicked = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.eventoDaVez = daVez;
		$location.path('app/eventounico');
		$rootScope.asInvited = true;
	};

	$scope.clickToView = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.eventoDaVez = daVez;
 		$scope.evtInfo = $rootScope.eventoDaVez;
		$scope.openModal();
	};

     $ionicModal.fromTemplateUrl('templates/auxiliar/view_event.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });
    
    
   $scope.goneFly = function(){
      console.log("Gonna fly now!");
      $scope.closeModal()
      $location.path("app/editaevento")
   }


   $scope.openModal = function() {
       $scope.modal1.show();
   };
    
   $scope.closeModal = function() {
       $scope.modal1.hide();
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });


   function showContent() {
   	// body...
	   $timeout(function(){
			$scope.timePaster = true;
			console.log("demorou...")
	   }, 1000);
   }



    ionicMaterialInk.displayEffect();
}; // fim da função
})(); // fim do documento
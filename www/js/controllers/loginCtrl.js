(function() {
'use_strict';

angular.module('APPlanner').controller('loginCtrl', loginCtrl);

function loginCtrl($scope, $rootScope, $location, $stateParams, $timeout, $firebaseObject, FireAuth, Utils, $localStorage){
    // Form data for the login modal
    $scope.user = {
      email: '',
      password: ''
    };

    $rootScope.isLogged = false;
    $rootScope.hider = true;

		$scope.$on("$ionicView.enter", function(event, data){
		     // handle event
         console.log("LogCtrl começa");
        //$scope.$parent.hideHeader();
		  });
    $scope.comparate = function(){
      if($localStorage.usuario){
        $location.path("app/calendario");
        console.log("tem credenciais")
      }else{ 
        $rootScope.hider = false;
          console.log("não tem credenciais")
      }
    }

    $scope.registro_simples = function(usuario){
      console.log(usuario)
      if(usuario.email != "" && usuario.password  != ""){
          $scope.user.email = usuario.email;
          $scope.user.password = usuario.password;
          console.log("está ok")
          FireAuth.register($scope.user);
  
      }else{
        console.log("Nada está ok")
          Utils.hide();
          Utils.alertshow("Erro", "Insira os dados corretamente")
      }
    }
    

    $scope.login_simples = function(usuario){
      $scope.user.email = usuario.email;
      $scope.password = usuario.password;
      FireAuth.login(usuario);
    }  


    $scope.faceLogin = function(){
      console.log("faceLogin")
      FireAuth.signFB();
    }

    $scope.retrievepass = function(){
      $location.path("/eventos");     
    }

    $scope.directUser = function(){
      $location.path("app/eventos");
    };

  $scope.comparate();

}; // fim da função
})(); // fim do documento
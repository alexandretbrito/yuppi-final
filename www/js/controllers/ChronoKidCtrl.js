(function() {
'use_strict';

angular.module('APPlanner').controller('ChronoKidCtrl', ChronoKidCtrl);

function ChronoKidCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, $filter, $timeout, Utils, $ionicPopup){

  $scope.$parent.showHeader();

   $scope.$on("$ionicView.enter", function(event, data){
        console.log("começo")
        //console.log($localStorage.eventos[$scope.theEvent].chron);
      if($rootScope.asInvited){
         $timeout(function(){
             if($localStorage.colabs[$scope.theEvent].chron == undefined){
                console.log("não tem")
                $scope.firstContact();
            }
           else{
                console.log("tem")
                $scope.secondContact();
            }
         }, 1000);
      }else{
         $timeout(function(){
             if($localStorage.eventos[$scope.theEvent].chron == undefined){
                console.log("não tem")
                $scope.firstContact();
            }
           else{
                console.log("tem")
                $scope.secondContact();
            }
         }, 1000);        
      }

   });

    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;


    $scope.retroMes = true;
    $scope.indexed = 0;

    $scope.listCheck = [];
    $scope.actualTab = "pre";
    $scope.restamMeses = 0;

    $scope.timeBefore = [];

    $scope.listPreKid = []; //lista de Pre Wedding
    $scope.listFestaKid = [];//Lista da Festa

    document.addEventListener("deviceready", function () {
     $scope.connec = $cordovaNetwork.isOnline()
     console.log($scope.connec)
     console.log("eu sou uma festa infantil")
    }); 

        $scope.showTaskPrompt = function() {
          if($scope.actualTab === "pre"){
            var newShop = {
                item: '',
                time: 1,
                done: false
            };
          }else{
            var newShop = {
                item: '',
                time: "18:00",
                done: false
            };

              var chop = $scope.modifItem.time;
              var arr = chop.split(':');
              var hr = arr[0];
              var mn = arr[1];
              var saveData = new Date(0);
              saveData.setHours(hr);
              saveData.setMinutes(mn);
              $scope.itemTime = {
                  value: saveData
              }; 
          }

            $scope.newShop = newShop;

            console.log($scope.itemTime);

            $scope.theBitsi = {};
            //$scope.hora = $scope.newShop.time;
            $scope.theBitsi.meses = $scope.newShop.time;
            $scope.theBitsi.values = $scope.timeBefore;
   

            $scope.openModal(1);
        };

        $scope.saveTask = function(indice) {
          if($scope.newShop.item != ''){
                console.log("gravou")
                if($scope.actualTab === "pre"){
                  console.log($scope.theBitsi.meses +" meses faltantes")
                  $scope.newShop.time = $scope.theBitsi.meses;
                  $scope.listPreKid.push($scope.newShop);              
                }
                if($scope.actualTab === "party"){
                  var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
                  $scope.newShop.time = datafinal;
                  console.log($scope.newShop)
                  $scope.listFestaKid.push($scope.newShop);
                }            
                $scope.closeModal(indice);
                $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do item")
          }
        };

        $scope.cancelTask = function(indice) {
            $scope.closeModal(indice);
        };

        $scope.firstContact = function(){
          console.log("------------------------------------------------------------")
          console.log("                      PRIMEIRO CONTATO                      ")
          console.log("------------------------------------------------------------")
          // Primeiro o PreWeeding
          var objPreKid = {};
          objPreKid = kidEntrega;
          //objPreKid = extraData.getPreWed();
          var objParty = {};
          objParty = kidFesta;
          //objParty = extraData.getParty();
          var all = {};
          all.pre = objPreKid;
          all.party = objParty;

          angular.forEach(all.pre, function(shops, key){
              $scope.listPreKid[key] = angular.copy(shops);
              console.log($scope.listPreKid)
          });
          //por último a festa
          angular.forEach(all.party, function(shops, key){
              shops.time = "18:00";
              console.log("---------------------------")
              console.log(shops)
              $scope.listFestaKid[key] = angular.copy(shops);
              $scope.listFestaKid[key].time = "18:00";
              
          });
          // gravar no $localStorage
          if($rootScope.asInvited){
            $localStorage.colabs[$scope.theEvent].chron = all;
          }else{
            $localStorage.eventos[$scope.theEvent].chron = all;
          }
          //gravar no Firebase        
          all = angular.copy(all)
          $scope.gravacao(all);
        };


        $scope.secondContact = function(){
          console.log("------------------------------------------------------------")
          console.log("                      SEGUNDO CONTATO                       ")
          console.log("------------------------------------------------------------")
          // Primeiro o PreWeeding
          var all = {};
          if($rootScope.asInvited){
            all = $localStorage.colabs[$scope.theEvent].chron;
          }else{
            all = $localStorage.eventos[$scope.theEvent].chron;
          }

          angular.forEach(all.pre, function(shops, key){
              $scope.listPreKid[key] = angular.copy(shops);
              console.log($scope.listPreKid)
          });
          //por último a festa
          angular.forEach(all.party, function(shops, key){
              $scope.listFestaKid[key] = angular.copy(shops);
              console.log($scope.listFestaKid)
          });
          // gravar no $localStorage
          if($rootScope.asInvited){
            $localStorage.colabs[$scope.theEvent].chron = all;            
          }else{
            $localStorage.eventos[$scope.theEvent].chron = all;            
          }
          $scope.grave_me(all);
        }


  $scope.tabSelected = function(tab) {
    console.log(tab + ' Tab Selected');
    $scope.actualTab = tab;
    if($scope.actualTab === "pre"){

    }
    if($scope.actualTab === "party"){
          
    }
  };



///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].chron = $scope.listCheck;          
        }else{
          $localStorage.eventos[$scope.theEvent].chron = $scope.listCheck;          
        }
        $scope.insertIntoObj();
    }

    $scope.removeMe = function(itim){
        console.log(itim)
      if($scope.actualTab === "pre"){
          var aquele = $scope.listPreKid.indexOf(itim)
          console.log(aquele)
          $scope.listPreKid.splice(aquele, 1)
      }
      if($scope.actualTab === "party"){
          var aquele = $scope.listFestaKid.indexOf(itim)
          console.log(aquele)
          $scope.listFestaKid.splice(aquele, 1)          
      }
        $scope.insertIntoObj();
    };

  $scope.delEvento = function(){
     // A confirm dialog
     var confirmPopup = $ionicPopup.confirm({
       title: 'Deletar',
       template: 'Tem certeza que quer deletar?',
       cssClass: 'my-popup',
       buttons: [
         { text: 'Não',
            type: 'button-assertive myred',
          },
       {
         text: 'Sim',
         type: 'button-positive-900',
         onTap: function() { 
           console.log('You are sure');
         $scope.removefromUpdate(2)        
         }
       }
      ]
     })

     confirmPopup.then(function(res) {
      console.log(res)
       if(res) {

       } else {
         console.log('You are not sure');
       }
     });
  };   


    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
    if($scope.actualTab === "pre"){
        $scope.listPreKid.splice($scope.indexed, 1)
    }
    if($scope.actualTab === "party"){
        $scope.listFestaKid.splice($scope.indexed, 1)          
    }
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(item){
        console.log(item);
        console.log($scope.actualTab);
        var indexed = 0;
        if($scope.actualTab === "pre"){
          indexed = $scope.listPreKid.indexOf(item);
          if($scope.listPreKid[indexed].done === false){
              $scope.listPreKid[indexed].done = true;
          }else{$scope.listPreKid[indexed].done = false}
        }
        if($scope.actualTab === "party"){
          indexed = $scope.listFestaKid.indexOf(item);
          if($scope.listFestaKid[indexed].done === false){
              $scope.listFestaKid[indexed].done = true;
          }else{$scope.listFestaKid[indexed].done = false}              
        }
        console.log("cliquei")
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
        console.log("fui acionada");
        if($scope.connec){
            console.log("vou gravar na internet")
            if($rootScope.asInvited){
              planFactory.insertCrono($rootScope.eventoDaVez.uid, $scope.theEvent, objeto)
            }else{
              planFactory.insertCrono($localStorage.usuario.uid, $scope.theEvent, objeto)
            }
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }

    $scope.gravacao = function(objeto){
        // body...
        console.log("fui acionada");
        if($scope.connec){
            console.log("vou gravar na internet")
            if($rootScope.asInvited){
              planFactory.insertCrono($rootScope.eventoDaVez.uid, $scope.theEvent, objeto)
            }else{
              planFactory.insertCrono($localStorage.usuario.uid, $scope.theEvent, objeto)
            }
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        if($scope.actualTab === "pre"){
          angular.forEach($scope.listPreKid, function(shopes, keyo){
              bje[keyo] = $scope.listPreKid[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
          if($rootScope.asInvited){
            $localStorage.colabs[$scope.theEvent].chron.pre = bje;
          }else{
            $localStorage.eventos[$scope.theEvent].chron.pre = bje;
          }
        }
        if($scope.actualTab === "party"){
          angular.forEach($scope.listFestaKid, function(shopes, keyo){
              bje[keyo] = $scope.listFestaKid[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
          if($rootScope.asInvited){
            $localStorage.colabs[$scope.theEvent].chron.party = bje;               
          }else{
            $localStorage.eventos[$scope.theEvent].chron.party = bje;               
          }
        }
        var prox = {};
        if($rootScope.asInvited){
          prox = $localStorage.colabs[$scope.theEvent].chron;
        }else{
          prox = $localStorage.eventos[$scope.theEvent].chron;
        }
        $scope.grave_me(prox);        
    };


    $scope.editTask = function(index, obito){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        $scope.modifItem = {};
        $scope.theItsi = {};
        $scope.indiceMod = index;
        $scope.modifItem = obito;
        $scope.theItsi.meses = $scope.modifItem.time;
        $scope.theItsi.values = $scope.timeBefore;
        console.log("entrou")
        console.log($scope.modifItem)

        if($scope.actualTab === "pre"){
          $scope.indexed = $scope.listPreKid.indexOf(obito);
        }
        if($scope.actualTab === "party"){
           $scope.retroMes = false;
           $scope.indexed = $scope.listFestaKid.indexOf(obito);
           if($scope.modifItem.time == 0){
                  var datainicial = new Date(0);
                  $scope.itemTime = {
                   value: datainicial
                  };  
              }else{
                  var chop = $scope.modifItem.time;
                  var arr = chop.split(':');
                  var hr = arr[0];
                  var mn = arr[1];
                  var datainiciada = new Date(0);
                  datainiciada.setHours(hr);
                  datainiciada.setMinutes(mn);
                  $scope.itemTime = {
                      value: datainiciada
                  };          
              }; 
        }


        $scope.openModal(2)
    };

 $scope.changeEventType = function(sel){
    console.log("changeEventType")
    console.log(sel);
    $scope.restamMeses = sel;
  }      

    $scope.updateTask = function(indice) {
        console.log("saiu")
          if($scope.actualTab === "pre"){
            $scope.modifItem.time = $scope.restamMeses;
            $scope.listPreKid[$scope.indexed] = $scope.modifItem;
          }
          if($scope.actualTab === "party"){
            console.log($scope.itemTime.value)
            var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
            $scope.modifItem.time = datafinal;
            console.log($scope.modifItem)
            $scope.listFestaKid[$scope.indexed] = $scope.modifItem;
            $scope.listFestaKid[$scope.indexed] = $scope.modifItem;               
          }
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };

     $ionicModal.fromTemplateUrl('templates/auxiliar/inserecronogramacasorio.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-right'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/modificacronogramacasorio.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-right'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }else{
       $scope.modal2.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }else{
       $scope.modal2.hide();
      }
   
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });


  $scope.medetempo = function(hiato){
    var tempoFaltante = '';
    if(hiato == 0.25){
      tempoFaltante = "uma semana antes";
    }
    else if(hiato == 0.5){
     tempoFaltante = "duas semanas antes"; 
    }
    else if(hiato == 0.75){
     tempoFaltante = "três semanas antes"; 
    }
    else if(hiato == 0){
     tempoFaltante = "não definido"; 
    }
    else if(hiato == 1){
     tempoFaltante = "um mês antes"; 
    }
    else{
     tempoFaltante = hiato+"meses antes"; 
    }

    return tempoFaltante;
  }




    // Festa Infantil
    var kidEntrega = {
      0:{
        item: 'Móveis',
        time: .25,
        done: false
      },
      1:{
        item: 'Bolo',
        time: .25,
        done: false
      },
      2:{
        item: 'Doces',
        time: .25,
        done: false
      },
      3:{
        item: 'Lembranças',
        time: .25,
        done: false
      }
    };

    var kidFesta = {
      1:{
        item: 'Chegada do Bolo',
            time: "18:00",
        done: false
      },
      2:{
        item: 'Chegada dos Doces',
            time: "18:00",
        done: false
      },
      3:{
        item: 'Chegada das Lembranças',
            time: "18:00",
        done: false
      },
      4:{
        item: 'Início da festa',
            time: "18:00",
        done: false
      },
      5:{
        item: 'Animação/Recreação',
            time: "18:00",
        done: false
      },
      6:{
        item: 'Personagem Vivo',
            time: "18:00",
        done: false
      },
      7:{
        item: 'Parabéns',
            time: "18:00",
        done: false
      },
      8:{
        item: 'Encerramento',
            time: "18:00",
        done: false
      },
      9:{
        item: 'Desmontagem',
            time: "18:00",
        done: false
      }   
    };


    var antecipacao = [
      {tempo: 0, label:"não definido"},
      {tempo: .25, label:"1 semana antes"},
      {tempo: .50, label:"2 semanas antes"},
      {tempo: .75, label:"3 semanas antes"},   
      {tempo: 1, label:"1 mês antes"},
      {tempo: 2, label:"2 meses antes"},
      {tempo: 3, label:"3 meses antes"},
      {tempo: 4, label:"4 meses antes"},
      {tempo: 5, label:"5 meses antes"},
      {tempo: 6, label:"6 meses antes"},
      {tempo: 7, label:"7 meses antes"},
      {tempo: 8, label:"8 meses antes"},
      {tempo: 9, label:"9 meses antes"},
      {tempo: 10, label:"10 meses antes"},
      {tempo: 11, label:"11 meses antes"},
      {tempo: 12, label:"12 meses antes"},
      {tempo: 13, label:"13 meses antes"},
      {tempo: 14, label:"14 meses antes"},
      {tempo: 15, label:"15 meses antes"},
      {tempo: 16, label:"16 meses antes"},
      {tempo: 17, label:"17 meses antes"},
      {tempo: 18, label:"18 meses antes"}
    ];

      //$scope.timeBefore = extraData.getTimeEarly();
      $scope.timeBefore = antecipacao;

}; // fim da função
})(); // fim do documento
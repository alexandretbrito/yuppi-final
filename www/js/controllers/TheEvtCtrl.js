(function() {
'use_strict';

angular.module('APPlanner').controller('TheEvtCtrl', TheEvtCtrl);

function TheEvtCtrl($scope, $rootScope, ionicMaterialInk, $ionicPopup, $ionicModal, $firebaseArray, $localStorage, $cordovaNetwork, planFactory, accessFactory, Utils, $location, $timeout){

	console.log($rootScope.eventoDaVez);
	$scope.nome = "";
//	$scope.nome = $rootScope.eventoDaVez.name;
  $scope.colaboradores = [];
  $scope.theColabs = []
  $scope.indexed = 0;
  $scope.newColab = {};
  $scope.prepared = false;
  $scope.pago = false;

  $scope.evtInfo = $rootScope.eventoDaVez;


  $scope.$on("$ionicView.enter", function(event, data){
  $scope.nome = $rootScope.eventoDaVez.name;
  console.log($scope.nome)
  $scope.pago = $rootScope.usuarioAtivo.pagante;
  console.log("o usuário deixou pago? "+ $scope.pago)
    if(!$rootScope.eventoDaVez.hasGroup){
      console.log("Não está cadastrado um grupo")
      $scope.hasGroup = false;
    }else{
      $scope.hasGroup = $rootScope.eventoDaVez.hasGroup;
    }
    bePrepared()
  });

  $scope.$on("$ionicView.leave", function(event, data){
      $scope.prepared = false;
  })

  document.addEventListener("deviceready", function () {
   $scope.connec = $cordovaNetwork.isOnline()
   console.log($scope.connec)
  });


	$scope.cronSelector = function(){
		if($rootScope.eventoDaVez.evtType == "Casamento"){
			console.log("é um casamento!");
			$location.path("app/cronogramacasorio");
		}else if($rootScope.eventoDaVez.evtType == "Festa Infantil"){
			console.log("é um aniversário de criança!");
			$location.path("app/cronogramainfantil");
		}else{
			console.log("é um evento normal");
			$location.path("app/cronograma");
		}
	}

  $scope.clickToView = function(){
    $scope.evtInfo = $rootScope.eventoDaVez;
    console.log("click to view")
    $scope.openModal(1);
  };



     $ionicModal.fromTemplateUrl('templates/auxiliar/view_event.html', {
      scope: $scope,
      id: '1',
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

      $ionicModal.fromTemplateUrl('templates/auxiliar/gerenciagrupos.html', {
      scope: $scope,
      id: '2',
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

      $ionicModal.fromTemplateUrl('templates/auxiliar/employeedata.html', {
      scope: $scope,
      id: '3',
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal3 = modal;
   });

      $ionicModal.fromTemplateUrl('templates/auxiliar/editEmployeedata.html', {
      scope: $scope,
      id: '4',
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal4 = modal;
   });


   $scope.goneFly = function(){
      console.log("Gonna fly now!");
      $scope.closeModal(1)
      $location.path("app/editaevento")
   }

   /// grupos

   $scope.makeGroup = function(){
      if($scope.connec){
        $rootScope.eventoDaVez.hasGroup = true;
        var recoded = angular.copy($rootScope.eventoDaVez)
        $localStorage.eventos[$rootScope.eventoDaVez.ID] = recoded;
        planFactory.insertEvt($rootScope.usuarioAtivo.uid, $rootScope.eventoDaVez.ID, recoded)
        var meuGrupo = {}
        meuGrupo.owner = $rootScope.usuarioAtivo.uid;
        meuGrupo.ID = $rootScope.eventoDaVez.ID;
        console.log(meuGrupo)
        planFactory.makeGroup($rootScope.eventoDaVez.ID, meuGrupo)
        Utils.alertshow("Sucesso", "Um grupo foi criado. Nele, você pode compartilhar os dados sobre este evento.")
        $scope.hasGroup = true;
      }else{
        Utils.alertshow("Erro", "É preciso estar conectado à internet.");
      }
   }

   $scope.deleteGroup = function(){
       var confirmPopup01 = $ionicPopup.confirm({
         title: 'Deletar',
         template: 'Tem certeza que quer excluir o grupo?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() {
             console.log('You are sure');
              $scope.subDeleteGroup()
           }
         }
        ]
       })
  };

  $scope.subDeleteGroup = function(){
      if($scope.connec){
        $rootScope.eventoDaVez.hasGroup = false;
        var recoded = angular.copy($rootScope.eventoDaVez)
        $localStorage.eventos[$rootScope.eventoDaVez.ID] = recoded;
        planFactory.insertEvt($rootScope.usuarioAtivo.uid, $rootScope.eventoDaVez.ID, recoded)
        planFactory.delGroup($rootScope.eventoDaVez.ID)
        Utils.alertshow("Sucesso", "O grupo deste evento foi excluído. Ninguém mais compartilha dados deste evento.")
        $scope.closeModal(2);
        $scope.hasGroup = false;
      }else{
        Utils.alertshow("Erro", "É preciso estar conectado à internet.");
      }
   }

  $scope.manageGroup = function(){
    var result = {};
    var osCaras = {};
    var i = 0;
    $scope.colaboradores = [];
    var eles = accessFactory.pegaTudo();
    var moreThey = eles.ref("grupos/"+$rootScope.eventoDaVez.ID);
    moreThey.once("value").then(function(snapshot){
      result = snapshot.val()
      console.log(result);
      if(result.colaboradores){
        osCaras = result.colaboradores;
        $scope.haveColabs = true;
        angular.forEach(osCaras, function(shops, key){
            console.log(i)
            $scope.colaboradores[i] = shops;
            i++;
            console.log($scope.colaboradores)
        });
      }else{
        $scope.haveColabs = false;
        ;
      }
    })
    firstMove();
  }

  $scope.insertColab = function(marcacao){
    var arquivo = {}
    var i = 0;
    console.log("Email inserido = "+ marcacao.mail)
    console.log("Seu email = "+ $rootScope.usuarioAtivo.email)
    if (marcacao.mail === $rootScope.usuarioAtivo.email) {
      Utils.alertshow("Erro", "Esse é o seu próprio email!")
    } else {
      var mojo = planFactory.getUsers();
      var chapas = $firebaseArray(mojo);
      chapas.$loaded().then(function(){
         angular.forEach(chapas, function(chapa) {
            if(chapa.email == marcacao.mail){
              arquivo = angular.copy(chapa)
              //arquivo.email = chapa.email;
              //arquivo.uid = chapa.uid;
              //if(chapa.displayName != ""){
                //arquivo.displayName = chapa.displayName;
              //}
              recordColab(arquivo, chapa.uid)
              Utils.alertshow("Sucesso", "Email inserido")
              i++;
            }
        })
        if(i == 0){
          Utils.alertshow("Erro", "Usuário não encontrado")
        }
      })
      $scope.newColab.mail = null;
    }
  }

  $scope.editColab = function(index, colaborador){
    $scope.indexed = index;
    console.log($scope.indexed)
    $scope.colabToEdit = colaborador;
    console.log($scope.colabToEdit)
    $scope.openModal(4)
  }

  $scope.saveColabEdition = function(num){
    $scope.closeModal(num);
    $scope.colabToEdit = angular.copy($scope.colabToEdit);
    recordColab($scope.colabToEdit, $scope.colabToEdit.uid)
  }

  $scope.cancelEdition = function(num){
    $scope.closeModal(4);
    $scope.indexed = 0;
    $scope.colabToEdit = {};
  }

  function recordColab(obj, uid){
    console.log(obj)
    delete obj.$id;
    delete obj.$priority;
    console.log(obj)
    var grupo= planFactory.getGroup($rootScope.eventoDaVez.ID);
    var gangue = grupo.child("colaboradores/"+uid);
    gangue.update(obj);
    colabTriage();
  }

  $scope.delColab = function(uid){
       var confirmPopup02 = $ionicPopup.confirm({
         title: 'Deletar',
         template: 'Tem certeza que quer deletar o colaborador?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() {
             console.log('You are sure to remove individual');
              $scope.subDelColab(uid)
           }
         }
        ]
       })
    }

  $scope.subDelColab = function(uid){
    var grupo= planFactory.getGroup($rootScope.eventoDaVez.ID);
    var gangue = grupo.child("colaboradores/"+uid);
    gangue.remove();
    colabTriage();
  }

  $scope.runAwayGroup = function(){
        var confirmPopup03 = $ionicPopup.confirm({
         title: 'Sair do Grupo',
         template: 'Tem certeza que deseja retirar-se como colaborador deste evento?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() {
             console.log('You are sure');
              $scope.autoRemoveColab()
           }
         }
        ]
       })
  };

  $scope.autoRemoveColab = function(){
    var grupo= planFactory.getGroup($rootScope.eventoDaVez.ID);
    var gangue = grupo.child("colaboradores/"+$rootScope.usuarioAtivo.uid);
    gangue.remove();
    delete $localStorage.colabs[$rootScope.eventoDaVez.ID];
   
    //$location.path("app/grupo");
  }

  function colabTriage(){
    // pegar os usuários selecionados e mostrar
    i=0;
    var results = {}
    var grupo= planFactory.getGroup($rootScope.eventoDaVez.ID);
    grupo.once("value").then(function(snapshot){
      results = snapshot.val().colaboradores
      console.log("results ok");
      if(results){
        $scope.colaboradores = [];
        $scope.haveColabs = true;
        angular.forEach(results, function(shopas, keyos){
            $scope.colaboradores[i] = shopas;
            i++;
            console.log($scope.colaboradores);
            $scope.closeModal(2);
            $scope.$apply();
      });
    }
    })
  };

  $scope.revealColab = function(mycolabor){
    $scope.spyed = mycolabor;
    $scope.openModal(3);
  }

  //////////////////////////////////////////////////////////////////////

   $scope.openModal = function(index) {
      console.log(index)
      if (index == 1){
       $scope.modal1.show();
      }if(index == 2){
       $scope.modal2.show();
      }
      if(index == 3){
       $scope.modal3.show();
      }
      if(index == 4){
       $scope.modal4.show();
      }
   };

   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }
      if (index == 2){
       $scope.modal2.hide();
      }
      if (index == 3){
       $scope.modal3.hide();
      }
      if (index == 4){
       $scope.modal4.hide();
      }
    };

   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function(index) {
      $scope.modal1.remove();
      $scope.modal2.remove();
      $scope.modal3.remove();
      $scope.modal4.remove();
   });

   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });

   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });


    function firstMove() {
      $timeout(function(){
      $scope.openModal(2);
      if($scope.haveColabs == false){
        Utils.alertshow("Gerenciar Grupos", "Para permitir que outras pessoas visualizem seu evento, você deve inserir o email de uma ou mais pessoas cadastradas no Yuppi. Assim, elas poderão seguir seu planejamento.")
      }
      }, 1200);
    }




    $scope.dialNumber = function(number) {
      window.open('tel:' + number, '_system');
    }


    function bePrepared() {
      $timeout(function(){
        mountContent();
      }, 600);
    }

    function mountContent(){
      $scope.prepared = true;
    }

    ionicMaterialInk.displayEffect();
}; // fim da função
})(); // fim do documento

(function() {
'use_strict';

angular.module('APPlanner').controller('NewEvtCtrl', NewEvtCtrl);

function NewEvtCtrl($scope, $rootScope, $localStorage, Utils, ionicToast, $location, planFactory, $cordovaNetwork, $filter, $timeout, $cordovaContacts, $ionicModal){
   $scope.$parent.showHeader();
   var permissions = cordova.plugins.permissions;
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);
    $scope.search = {};

	$scope.$on("$ionicView.enter", function(event, data){
	   $scope.evtPreCad = true;
	   $scope.naocasorio = true;
	   $scope.mySelect = "Clique para escolher";
		$scope.evtType = "";
		$scope.newEvt = {};
		$scope.allEvents = {};

		$scope.newEvt = {
			client_name: "",
			client_phone: "",
			client2_name: "",
			client2_phone: "",
			cont01Nome: "",
			cont01Phone: "",
			cont02Nome: "",
			cont02Phone: "",
			cont03Nome: "",
			cont03Phone: "",
			cont04Nome: "",
			cont04Phone: "",
			client_name_comp:"",
			client_ender:"",
			client_ender2:"",
			client_neigh:"",
			client_city:"",
			client_uf:"",
			client_cep:"",
			client_cpf:"",
			client_rg:"",
			client_meet_days:"",
			client_meet_hour:"",
			unique_invites: false
		}
	});

	document.addEventListener("deviceready", function () {
		$scope.connec = $cordovaNetwork.isOnline()
		console.log($scope.connec)
		prepare_perm();
	   }); 

	   function prepare_perm() {
		 // body...
		   permissions.checkPermission(permissions.READ_CONTACTS, function( status ){
		   if ( status.hasPermission ) {
			 console.log("Yes :D ");
		   }
		   else {
			 console.log("No :( ");
			 permissions.requestPermission(permissions.READ_CONTACTS, success, error);
			  
			 function error() {
			   console.log('Contacts permission is not turned on');
			 }
			  
			 function success( status ) {
			   if( !status.hasPermission ) error();
			 }          
		   }
			});
	   };


	    $scope.$on("$ionicView.leave", function(event){
		$scope.dataDoEvento = null;
		$scope.horaDoEvento = null;
		$scope.eventer = {
			otherEvtType: ""
		};	
		delete $scope.itemTime.value;
		delete $scope.churchTime.value;
		delete $scope.partyTime.value;
		$scope.itemDate = new Date();
		$scope.evtType = "";
		$scope.NewEvtCtrl = null;
		$scope.allEvents = null;
        console.log("saí daqui"); 
	    });	

		$scope.newEvt = {};
		$scope.allEvents = {};
		if($localStorage.eventos){
			$scope.allEvents = $localStorage.eventos;
			rewindEvt()
		};	


 $scope.changeEventType = function(sel){
  	console.log(sel);
  		$scope.evtType = sel; 
  	if(sel == "Outro"){
  		$scope.evtPreCad = false;
  		$scope.naocasorio = true;	
  	}
  	 else if(sel == "Casamento"){
  		$scope.naocasorio = false;
  		$scope.evtPreCad = true;		
  	}else{
  		$scope.naocasorio = true;
  		$scope.evtPreCad = true;
  	}
  }

  function rewindEvt(argument) {
  	// body...
 		console.log("aqui está o evento já guardado")
		console.log($scope.allEvents); 	
  }


/////////////////////////////////////////////////////////
//					Objetos de Tempo
/////////////////////////////////////////////////////////

	var timeSeed = "18:00";

	var arr = timeSeed.split(':');
	var hr = arr[0];
	var mn = arr[1];
	var saveData = new Date(0);
	saveData.setHours(hr);
	saveData.setMinutes(mn);

	var saveDay = new Date();

	$scope.itemTime = {
    	value: saveData
    	}; 
	$scope.churchTime = {
    	value: saveData
    }; 
	$scope.partyTime = {
    	value: saveData
   	}; 

    $scope.itemDate = {
     	value: saveDay
   	};

//////////////////////////////////////////////////////////

function engMonth(mes){
var abrev = "";
if(mes === "01"){
	abrev = "Jan"; 
}else if(mes === "02"){
	abrev = "Feb";	
}else if(mes === "03"){
	abrev = "Mar";	
}else if(mes === "04"){
	abrev = "Apr";	
}else if(mes === "05"){
	abrev = "May";	
}else if(mes === "06"){
	abrev = "Jun";	
}else if(mes === "07"){
	abrev = "Jul";	
}else if(mes === "08"){
	abrev = "Aug";	
}else if(mes === "09"){
	abrev = "Sep";	
}else if(mes === "10"){
	abrev = "Oct";	
}else if(mes === "11"){
	abrev = "Nov";	
}else if(mes === "12"){
	abrev = "Dec";	
}

return abrev;

}



var ID = function () {
  return 'APP' + Math.random().toString(36).substr(2, 9);
};


$scope.vambora = function(){
		$location.path('app/eventos');
}

$scope.marcador = function(marcacao){
	var codice = marcacao;
	if(codice.name == undefined || codice.place == undefined || codice.client_name == undefined || codice.client_phone == undefined){
			Utils.alertshow("Falha no preenchimento mínimo", "Alguma das áreas marcadas com asteriscos não foram preenchidas.")
	}else{
		if(codice.cont01Nome != undefined && codice.cont01Phone == undefined || codice.cont01Nome == undefined && codice.cont01Phone != undefined){
			Utils.alertshow("Erro", "Primeiro contato extra incompleto")
		}
		else if(codice.cont02Nome != undefined && codice.cont02Phone == undefined || codice.cont02Nome == undefined && codice.cont02Phone != undefined){
			Utils.alertshow("Erro", "Segundo contato extra incompleto")
		}
		else if(codice.cont03Nome != undefined && codice.cont03Phone == undefined || codice.cont03Nome == undefined && codice.cont03Phone != undefined){
			Utils.alertshow("Erro", "Terceiro contato extra incompleto")
		}
		else if(codice.cont04Nome != undefined && codice.cont04Phone == undefined || codice.cont04Nome == undefined && codice.cont04Phone != undefined){
			Utils.alertshow("Erro", "Quarto contato extra incompleto")
		}
		else{
			$scope.submarcador(codice)
		}	
	}

}

$scope.submarcador = function(marcacao2){	

	var horaevento = $filter('date')($scope.itemTime.value, 'HH:mm');
	console.log(horaevento);
	var dataEvento = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');
	console.log(dataEvento);

	var marker = marcacao2;
	var refined_evt = {};
	var shaca = dataEvento;
	var utcMes = shaca.split("/");
	var diaCal = utcMes[0];
	var anoCal = utcMes[2];
	var mesCal =  utcMes[1];
	var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;


	refined_evt.evtHour = horaevento;
	refined_evt.evtDay = dataEvento;
	refined_evt.calId = calId;

	//marker.ID = ID();
	refined_evt.ID = ID();	

	if($scope.evtType == "Casamento"){
		var horaCasorio = $filter('date')($scope.churchTime.value, 'HH:mm');
		console.log("Casa às "+horaCasorio);
		//marker.horaCasorio = horaCasorio;
		refined_evt.horaCasorio = horaCasorio;
		var horaFesta = $filter('date')($scope.partyTime.value, 'HH:mm');
		console.log("Festa às "+horaCasorio);		
		//marker.horaFesta = horaFesta;
		refined_evt.horaFesta = horaFesta;
	}



	if($scope.evtType !== "Outro"){
		//marker.evtType = $scope.evtType;
		refined_evt.evtType = $scope.evtType;
	}
	if($scope.evtType == "Outro"){
		//marker.evtType = $scope.otherEvtType;
		console.log("É outro. Talvez seja "+marker.otherEvtType)
		refined_evt.evtType = marker.evtType;
	}
	if($scope.evtType == "Clique para escolher"){
		//marker.evtType = "";
		refined_evt.evtType = "";
	}

	//passando os outros para refined_evt

	refined_evt.client_name = marker.client_name;
	refined_evt.client_phone = marker.client_phone;
	refined_evt.name = marker.name;
	refined_evt.place = marker.place;
	if(marker.client_mail){
		refined_evt.client_mail = marker.client_mail;
	}
	//Co-resonsável
	if(marker.client2_name){
		refined_evt.client2_name = marker.client2_name;
	}
	if(marker.client2_phone){
		refined_evt.client2_phone = marker.client2_phone;
	}
	if(marker.client2_mail){
		refined_evt.client2_mail = marker.client2_mail;
	}		

	//Contato 1
	if(marker.cont01Nome){
		refined_evt.cont01Nome = marker.cont01Nome;
	}
	if(marker.cont01Phone){
		refined_evt.cont01Phone = marker.cont01Phone;
	}

	//Contato 2
	if(marker.cont02Nome){
		refined_evt.cont02Nome = marker.cont02Nome;
	}
	if(marker.cont02Phone){
		refined_evt.cont02Phone = marker.cont02Phone;
	}

	//Contato 3
	if(marker.cont03Nome){
		refined_evt.cont03Nome = marker.cont03Nome;
	}
	if(marker.cont03Phone){
		refined_evt.cont03Phone = marker.cont03Phone;
	}

	//Contato 4
	if(marker.cont04Nome){
		refined_evt.cont04Nome = marker.cont04Nome;
	}
	if(marker.cont04Phone){
		refined_evt.cont04Phone = marker.cont04Phone;
	}


	refined_evt = angular.copy(refined_evt)

	console.log(marker)
			refined_evt.client_name_comp = marker.client_name_comp;
			refined_evt.client_ender = marker.client_ender;
			refined_evt.client_ender2 = marker.client_ender2;
			refined_evt.client_neigh = marker.client_neigh;
			refined_evt.client_city = marker.client_city;
			refined_evt.client_uf = marker.client_uf;
			refined_evt.client_cep = marker.client_cep;
			refined_evt.client_cpf = marker.client_cpf;
			refined_evt.client_rg = marker.client_rg;
			refined_evt.client_meet_days = marker.client_meet_days;
			refined_evt.client_meet_hour = marker.client_meet_hour;

	console.log("////////////////")
	console.log(refined_evt)

	if($localStorage.eventos){
			acabe_de_gravar(refined_evt);	
	}else{
		$localStorage.eventos = {};
		acabe_de_gravar(refined_evt);
	}
	//evalEvents();
	Utils.show();

};


	

	function acabe_de_gravar(petardo) {
		// body...
		var objet = {};
		objet = petardo;
		console.log(objet)
		$localStorage.eventos[objet.ID] = objet;

		//console.log($localStorage.eventos)		

		if($scope.connec){
			console.log("vou gravar na internet")
			planFactory.insertEvt($localStorage.usuario.uid, objet.ID, objet)
		}else{
			console.log("não vou gravar na internet")
			ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
		}

		$timeout(function(){
			Utils.hide();
			$location.path('app/eventos');			
		}, 1000);


	}	

	$scope.captureContact = function(){
		$scope.closeModal();
		$scope.search = {};
	}

	// contatos direto do fone
	$scope.getContact = function(quem){
		$scope.who = quem;
		if($scope.who == "cliente"){
			$scope.phrase = "Clique para escolher o cliente de sua lista de contatos"
		}
		if($scope.who == "cliente2"){
			$scope.phrase = "Clique para escolher o cliente de sua lista de contatos"
		}
		if($scope.who == "cont01"){
			$scope.phrase = "Clique para escolher o contato do evento de sua lista de contatos"
		}
		if($scope.who == "cont02"){
			$scope.phrase = "Clique para escolher o contato do evento de sua lista de contatos"
		}
		if($scope.who == "cont03"){
			$scope.phrase = "Clique para escolher o contato do evento de sua lista de contatos"
		}
		if($scope.who == "cont04"){
			$scope.phrase = "Clique para escolher o contato do evento de sua lista de contatos"
		}

		$scope.openModal();
	    $cordovaContacts.find({filter : ''}).then(function(results){
	        $scope.contactos = results;
	        console.log(results)
	    }, function(error){
	        console.log(error);
	        Utils.alertshow("Erro", "Não foi possível carregar sua lista de contatos");
	        $scope.closeModal();
	    });

	};

	$scope.chooseContact = function(resulte){
		$scope.closeModal();
		console.log(resulte);
		console.log($scope.who);

			
		if($scope.who == "cliente"){
				$scope.newEvt.client_name = resulte.name.formatted;
				$scope.newEvt.client_phone = resulte.phoneNumbers[0].value;
		}
		if($scope.who == "cliente2"){
			$scope.newEvt.client2_name = resulte.name.formatted;
			$scope.newEvt.client2_phone = resulte.phoneNumbers[0].value;
		}
		if($scope.who == "cont01"){
			$scope.newEvt.cont01Nome = resulte.name.formatted;
			$scope.newEvt.cont01Phone = resulte.phoneNumbers[0].value;
		}
		if($scope.who == "cont02"){
			$scope.newEvt.cont02Nome = resulte.name.formatted;
			$scope.newEvt.cont02Phone = resulte.phoneNumbers[0].value;
		}
		if($scope.who == "cont03"){
			$scope.newEvt.cont03Nome = resulte.name.formatted;
			$scope.newEvt.cont03Phone = resulte.phoneNumbers[0].value;
		}
		if($scope.who == "cont04"){
			$scope.newEvt.cont04Nome = resulte.name.formatted;
			$scope.newEvt.cont04Phone = resulte.phoneNumbers[0].value;
		}
		console.log($scope.newEvt)


	}


     $ionicModal.fromTemplateUrl('templates/auxiliar/capturaeventer.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal = modal;
   });     

    
   $scope.openModal = function() {
        $scope.modal.show();
   };
    
   $scope.closeModal = function() {
        $scope.modal.hide();
  };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   }); 

	//  fim de contatos direto do fone

	$scope.editEvento = function(evt){
		$rootScope.editableEvt = evt;
	}


	$scope.monClick = function(){
		console.log("clique está em " + $scope.newEvt.unique_invites)
	}


}; // fim da função
})(); // fim do documento
(function() {
'use_strict';

angular.module('APPlanner').controller('EditEvtCtrl', EditEvtCtrl);

function EditEvtCtrl($scope, $rootScope, $localStorage, Utils, ionicToast, $location, planFactory, $cordovaNetwork, $filter, $timeout){
   $scope.$parent.showHeader();
   var permissions = cordova.plugins.permissions;
   $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

	$scope.$on("$ionicView.enter", function(event, data){
		$scope.evtType = "";
		$scope.editedEvt = {};
		$scope.allEvents = {};
		$scope.editedEvt = $rootScope.eventoDaVez;
		$scope.editedID = $scope.editedEvt.ID;
		console.log("carregado")
		console.log($scope.editedEvt)
	   $scope.evtPreCad = true;
	   if($scope.editedEvt.evtType == "Casamento"){
			$scope.naocasorio = false;
	   }else{
			$scope.naocasorio = true;
	   }
	   $scope.mySelect = "Clique para escolher";

		console.log($scope.editedEvt.evtHour +" em cima")
		spreadTime();
	});

	document.addEventListener("deviceready", function () {
		$scope.connec = $cordovaNetwork.isOnline()
		console.log($scope.connec)
		prepare_perm();
	   }); 

	   function prepare_perm() {
		 // body...
		   permissions.checkPermission(permissions.READ_CONTACTS, function( status ){
		   if ( status.hasPermission ) {
			 console.log("Yes :D ");
		   }
		   else {
			 console.log("No :( ");
			 permissions.requestPermission(permissions.READ_CONTACTS, success, error);
			  
			 function error() {
			   console.log('Contacts permission is not turned on');
			 }
			  
			 function success( status ) {
			   if( !status.hasPermission ) error();
			 }          
		   }
			});
	   };


	    $scope.$on("$ionicView.leave", function(event){
		$scope.dataDoEvento = null;
		$scope.horaDoEvento = null;
		$scope.otherEvtType = "";
		$scope.evtType = "";
		$scope.allEvents = null;
        console.log("saí daqui"); 
	    });	

		$scope.editedEvt = {};
		$scope.allEvents = {};
		if($localStorage.eventos){
			$scope.allEvents = $localStorage.eventos;
			rewindEvt()
		};	


 $scope.changeEventType = function(sel){
  	console.log(sel);
  		$scope.evtType = sel; 
  	if(sel == "Outro"){
  		$scope.evtPreCad = false;
  		$scope.naocasorio = true;	
  	}
  	 else if(sel == "Casamento"){
  		$scope.naocasorio = false;
  		$scope.evtPreCad = true;		
  	}else{
  		$scope.naocasorio = true;
  		$scope.evtPreCad = true;
  	}
  }

  function rewindEvt(argument) {
  	// body...
 		console.log("aqui está o evento já guardado")
		console.log($scope.allEvents); 	
  }


/////////////////////////////////////////////////////////
//					Objetos de Tempo
/////////////////////////////////////////////////////////

	function spreadTime(){
		// body...
	//horado evento
	var timeSeed = $scope.editedEvt.evtHour;
	var arr = timeSeed.split(':');
	var hr = arr[0];
	var mn = arr[1];
	var saveData = new Date(0);
	saveData.setHours(hr);
	saveData.setMinutes(mn);
	$scope.itemTime = {
    	value: saveData
	};
	console.log($scope.itemTime.value +" de horas") 

	var diaD = $scope.editedEvt.evtDay;
	var diaArr = diaD.toString().split('/');
	var finalCountDown = new Date(diaArr[2], diaArr[1], diaArr[0]);
    $scope.itemDate = {
     	value: finalCountDown
   	}; 

   	console.log($scope.itemDate.value)
   	// dia e hora do casamento
   	if($scope.editedEvt.evtType === "Casamento"){

	var timeSeed2 = $scope.editedEvt.horaCasorio;
		var arr2 = timeSeed2.split(':');
		var hr2 = arr2[0];
		var mn2 = arr2[1];
		var saveData2 = new Date(0);
		saveData2.setHours(hr2);
		saveData2.setMinutes(mn2);
		$scope.churchTime = {
	    	value: saveData2
	   	}; 

	var timeSeed3 = $scope.editedEvt.horaFesta;
		var arr3 = timeSeed3.split(':');
		var hr3 = arr3[0];
		var mn3 = arr3[1];
		var saveData3 = new Date(0);
		saveData3.setHours(hr3);
		saveData3.setMinutes(mn3);

		$scope.partyTime = {
	    	value: saveData3
	   	}; 
	   }
	}
//////////////////////////////////////////////////////////

function engMonth(mes){
var abrev = "";
if(mes === "01"){
	abrev = "Jan"; 
}else if(mes === "02"){
	abrev = "Feb";	
}else if(mes === "03"){
	abrev = "Mar";	
}else if(mes === "04"){
	abrev = "Apr";	
}else if(mes === "05"){
	abrev = "May";	
}else if(mes === "06"){
	abrev = "Jun";	
}else if(mes === "07"){
	abrev = "Jul";	
}else if(mes === "08"){
	abrev = "Aug";	
}else if(mes === "09"){
	abrev = "Sep";	
}else if(mes === "10"){
	abrev = "Oct";	
}else if(mes === "11"){
	abrev = "Nov";	
}else if(mes === "12"){
	abrev = "Dec";	
}

return abrev;

}


$scope.vambora = function(){
		$location.path('app/eventos');
}

$scope.marcador = function(marcacao){

	var horaevento = $filter('date')($scope.itemTime.value, 'HH:mm');
	console.log(horaevento);
	var dataEvento = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');
	console.log(dataEvento);

	var marker = marcacao;

	var shaca = dataEvento;

	var utcMes = shaca.split("/");
	var diaCal = utcMes[0];
	var anoCal = utcMes[2];
	var mesCal =  utcMes[1];
	var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;

	marker.evtHour = horaevento;
	marker.evtDay = dataEvento;
	marker.calId = calId;

	console.log(dataEvento);


	if($scope.evtType == "Casamento"){
		var horaCasorio = $filter('date')($scope.churchTime.value, 'HH:mm');
		console.log("Casa às "+horaCasorio);
		marker.horaCasorio = horaCasorio;
		var horaFesta = $filter('date')($scope.partyTime.value, 'HH:mm');
		console.log("Festa às "+horaCasorio);		
		marker.horaFesta = horaFesta;
	}

	marker = angular.copy(marker)

	acabe_de_gravar(marker);
	Utils.show();
};


	function acabe_de_gravar(petardo) {
		// body...
		var objet = {};
		objet = petardo;
		console.log(objet)
		$localStorage.eventos[objet.ID] = objet;

		//console.log($localStorage.eventos)		

		if($scope.connec){
			console.log("vou gravar na internet")
			planFactory.insertEvt($localStorage.usuario.uid, objet.ID, objet)
		}else{
			console.log("não vou gravar na internet")
			ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
		}

		$timeout(function(){
			Utils.hide();
			$location.path('app/eventounico');			
		}, 1000);


	}	 

	$scope.monClick = function(){
		console. log($scope.editedEvt)		
	}

}; // fim da função
})(); // fim do documento
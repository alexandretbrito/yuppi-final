(function() {
'use_strict';

angular.module('APPlanner').controller('ConvidadosCtrl', ConvidadosCtrl);

function ConvidadosCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, accessFactory, Utils, $ionicPopup, $timeout){
   $scope.$parent.showHeader();

    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;
    var objTodo = {};

    $scope.sorted = false;
    $scope.compler = {}

  $scope.convivas = 0;
  $scope.confirmados = 0;
  $scope.faltantes = 0;

    $scope.listCheck = [];

        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
        }); 

	$scope.$on("$ionicView.enter", function(event, data){

		if($scope.connec){
			  objTodo = {}
          if($rootScope.asInvited){
            var trans = accessFactory.pegaGuestList($localStorage.colabs[$scope.theEvent].uid);
          }else{
            var trans = accessFactory.pegaGuestList($localStorage.usuario.uid);
          }
            var traste = trans.child($scope.theEvent);
          traste.on("value", function(snapshot){
		        objTodo = snapshot.val();
		        if(objTodo !== null){
		        getGuests();
		    	}else{
		    	console.log("vazio");
					Utils.alertshow("Atenção!", "Não há convidados registrados. Registre convidados unitariamnte aqui, ou salve sua planilha Excel no formato CSV e faça upload pelo nosso site.")	
		    	}
		      });
	  	}else{
				Utils.alertshow("Atenção!", "Esta funcionalidade requer conexão à internet.")	
	  	};

	});

  $scope.sortButter = function(){
    $scope.sorted = !$scope.sorted;
  }


    function getGuests() {
      // body...
      if(objTodo != {}){
          var bjec = {};
          bjec = objTodo;
          angular.forEach(bjec, function(tods, key){

              $scope.listCheck[key] = angular.copy(tods);
              //console.log(bjec)
              $scope.somadasPartes()
          });
      }else{
          console.log("segue vazio");
      }
      $timeout(function(){
        $scope.$apply();
      }, 100);
    }


    $scope.showTaskPrompt = function() {
            console.log("showTaskPrompt")
            if($rootScope.eventoDaVez.unique_invites){
              console.log("Vemos incorporamos e verificamos se é criança")
              var newGuest = {
                  nome: '',
                  crianca: false,
                  done: false
              };
            }else{
              console.log("Normal")
              var newGuest = {
                  nome: '',
                  convidados: 1,
                  done: false
              };
            }  
            $scope.newGuest = newGuest;
            $scope.openModal(1);
        };


        $scope.saveTask = function() {
          if($scope.newGuest.nome != ''){
              console.log("gravou")
              $scope.listCheck.push($scope.newGuest);
              $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do convidado")
          }
          $scope.closeModal(1);
        };  

        $scope.cancelTask = function() {
            $scope.closeModal(1);
            $scope.newGuest = null;
        };


///////////////////////////////////////////////////////////////////////////////////

  $scope.delEvento = function(item){
     // A confirm dialog
     var itex = item
     var confirmPopup = $ionicPopup.confirm({
       title: 'Deletar',
       template: 'Tem certeza que quer deletar?',
       cssClass: 'my-popup',
       buttons: [
         { text: 'Não',
            type: 'button-assertive myred',
          },
       {
         text: 'Sim',
         type: 'button-positive-900',
         onTap: function() { 
           console.log('You are sure');
         $scope.removeMe(itex)        
         }
       }
      ]
     })

     confirmPopup.then(function(res) {
      console.log(res)
       if(res) {

       } else {
         console.log('You are not sure');
       }
     });
  }; 


    $scope.removeMe = function(itim){
      console.log("removeMe")
      var aquele = $scope.listCheck.indexOf(itim)
      console.log(aquele);
      $scope.listCheck.splice(aquele, 1)
      console.log($scope.listCheck);
        $scope.insertIntoObj();
    }

      $scope.doneClicked = function(item){
        var index = $scope.listCheck.indexOf(item)
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };  

    $scope.grave_me = function(objeto) {
        // body...
            planFactory.insertGuest($localStorage.usuario.uid, $scope.theEvent, objeto)
    }   

    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(todes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.grave_me(bje);
        $scope.somadasPartes();      
    };

     $ionicModal.fromTemplateUrl('templates/auxiliar/insereconvidado.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });
     $ionicModal.fromTemplateUrl('templates/auxiliar/editaconvidado.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });
    
   $scope.openModal = function(index) {
        console.log("open")
       if(index == 1){ 
        $scope.modal1.show();
       }else{
        $scope.modal2.show()
       } 
   };
    
   $scope.closeModal = function(index) {
        console.log("close")
       if(index == 1){  
         $scope.modal1.hide();
       }else{
          $scope.modal2.hide();
       }
   };

   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });   

///////////////////////////////////////////////////////////////////////////////////

    $scope.editTask = function(obito){
        $scope.indiceMod = $scope.listCheck.indexOf(obito);
        $scope.modifItem = {};
        backupItem = {};
        $scope.modifItem = obito;
        $localStorage.backup = {}
        $localStorage.backup.noms = obito.nome;
        if($rootScope.eventoDaVez.unique_invites){
          $localStorage.backup.crianca = obito.crianca;          
        }else{
          $localStorage.backup.convs = obito.convidados;
        }
        $localStorage.backup.dons = obito.done;
        console.log("entrou")
        console.log($scope.modifItem)
        console.log($scope.indiceMod)
        console.log($localStorage.backup)
        $scope.openModal(2)
    };

    $scope.notUpdateTask = function(){
        delete $scope.modifItem;
        console.log("saiu sem salvar");
        console.log($localStorage.backup);
        $scope.listCheck[$scope.indiceMod].nome = $localStorage.backup.noms;
        if($rootScope.eventoDaVez.unique_invites){
          $scope.listCheck[$scope.indiceMod].crianca = $localStorage.backup.crianca;          
        }else{
          $scope.listCheck[$scope.indiceMod].convidados = $localStorage.backup.convs;
        }        
        $scope.listCheck[$scope.indiceMod].done = $localStorage.backup.dons;
        delete $localStorage.backup;
        $scope.closeModal(2)
    }

    $scope.updateTask = function() {
        console.log("saiu")
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.insertIntoObj();
        delete $localStorage.backup;
        $scope.closeModal(2)
    };


///////////////////////////////////////////////////////////////////////////////////

    $scope.totalize_me = function(){
      var numino = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            numino = numino + todes.convidados
        });
        return numino;
    }

    $scope.totalize_unique = function(){
      var numino4 = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            numino4 = numino4 + 1;
        });
        return numino4;
    }

    $scope.confirme_unique = function(){
      var numino5 = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            if(todes.done == true){
              numino5 = numino5 + 1;
            }
        });
        return numino5;
    }

    $scope.confirme_me = function(){
      var numino2 = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            if(todes.done == true){
              numino2 = numino2 + todes.convidados;
            }
        });
        return numino2;
    }

    $scope.differ_me = function(){
      var parte_a = $scope.totalize_me();
      var parte_b = $scope.confirme_me();
        return parte_a - parte_b;
    }

     $scope.differ_unique = function(){
      var parte_a = $scope.totalize_unique();
      var parte_b = $scope.confirme_unique();
        return parte_a - parte_b;
    }

    $scope.criancas = function(){
      var numino3 = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            if(todes.crianca == true){
              numino3 = numino3 + 1;
            }
        });
        return numino3;
    }

    $scope.somadasPartes = function(){

      $scope.convivas = $scope.totalize_me();
      $scope.confirmados = $scope.confirme_me();
      $scope.faltantes = $scope.differ_me();
    }

    }; // fim da função
})(); // fim do documento
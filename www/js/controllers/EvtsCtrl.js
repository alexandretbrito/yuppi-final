(function() {
'use_strict';

angular.module('APPlanner').controller('EvtsCtrl', EvtsCtrl);

function EvtsCtrl($scope, $rootScope, $location, ionicMaterialInk, $localStorage, $ionicPopup, $ionicModal, Utils, $timeout, planFactory, accessFactory){
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $scope.tuttiEventi = [];
    $scope.timePaster = false;

	$scope.$on("$ionicView.enter", function(event, data){	 


		if($localStorage.eventos){
	  			$rootScope.allEvents = $localStorage.eventos;
	  			$scope.temEventos = true;
		  		evalEvents();
	  	}else{
	  	/// aqui tem o bring
			retrievEvt($localStorage.usuario.uid);


	  	/// aqui tem o watch	
	  		$scope.temEventos = false;
	  		console.log("zerado")
	  	}
	  	Utils.spinnerEvents();
  		showContent();
	});

		function retrievEvt(uid){
			console.log("retrievEvt")
			console.log(uid);
			var evPath = accessFactory.pegaEventList(uid);
			evPath.once('value').then(function(snapshot){
				console.log(snapshot.val())
				$localStorage.eventos = snapshot.val();
				$rootScope.allEvents = $localStorage.eventos;
				//$scope.sharedEvts();
				$scope.temEventos = true;
			});
		};	

	$scope.$on("$ionicView.leave", function(event, data){
			$scope.timePaster = false;
			console.log("the events are leaving the building...")		
	})	

		$scope.newEvt = {};
		$rootScope.allEvents = {};	


function evalEvents() {
		var i=0
        var bjec = {};
        var agora = new Date().getTime();
        $scope.tuttiEventi = [];
        bjec = $rootScope.allEvents;
        angular.forEach(bjec, function(shops, key){
            $scope.tuttiEventi[i] = angular.copy(shops);
			$scope.tuttiEventi[i].compareDate = datetoalign($scope.tuttiEventi[i].evtDay, $scope.tuttiEventi[i].evtHour);
			$scope.tuttiEventi[i].past = datetofade($scope.tuttiEventi[i].compareDate, agora);
            i++;
        });
        console.log("vai")
        console.log(bjec)
        console.log($scope.tuttiEventi);
        
}


	$scope.dia = function(evento){
		console.log("chegou o dia")
		var dengo = new Date(evento.evtDay);
		return dengo;
	}

        function datetoalign(evtdate, hora){
        	var dateArr = [];
	        dateArr = evtdate.split("/");
			var supertime = dateArr[2]+"-"+dateArr[1]+"-"+dateArr[0]+"T"+hora+":00";
			var numTime = new Date(supertime).getTime();
			return numTime;
        }

        function datetofade(datecomp, datenow){
        	var bool_state;
        	if(datecomp > datenow){
        		bool_state = true;
        	}else{
        		bool_state = false;
        	}
        	return bool_state;
        }


	$scope.clicked = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.eventoDaVez = daVez;
		$location.path('app/eventounico');
		$rootScope.asInvited = false;
	};

	$scope.delEvento = function(evento){
		var daVez = {};
		daVez = evento;
		 // A confirm dialog
	   var confirmPopup = $ionicPopup.confirm({
	     title: 'Deletar Evento',
	     template: 'Tem certeza que você quer deletar este evento?',
	     cssClass: 'my-popup',
	     buttons: [
   		   { text: 'Não',
   		   	  type: 'button-assertive myred',
   		    },
		   {
		     text: 'Sim',
		     type: 'button-positive-900',
		     onTap: function() { 
		       console.log('You are sure');
				console.log(daVez.ID)
				 $scope.deletaEvento(daVez.ID)    		
		     }
		   }
		  ]
	   });

	   confirmPopup.then(function(res) {
	   	console.log(res)
	     if(res) {

	     } else {
	       console.log('You are not sure');
	     }
	   });
	};

	$scope.mandapraNovo = function(evento){
		$location.path('app/novoevento');
	};

	$scope.deletaEvento = function(theId){
		delete $localStorage.eventos[theId];
		planFactory.removeEvt($rootScope.usuarioAtivo.uid , theId);
		planFactory.delGroup(theId)
		$rootScope.allEvents = $localStorage.eventos;
		evalEvents();
	}

	$scope.clickToView = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.eventoDaVez = daVez;
 		$scope.evtInfo = $rootScope.eventoDaVez;
		$scope.openModal();
	};

     $ionicModal.fromTemplateUrl('templates/auxiliar/view_event.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });
    
    
   $scope.goneFly = function(){
      console.log("Gonna fly now!");
      $scope.closeModal()
      $location.path("app/editaevento")
   }


   $scope.openModal = function() {
       $scope.modal1.show();
   };
    
   $scope.closeModal = function() {
       $scope.modal1.hide();
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });


   function showContent() {
   	// body...
	   $timeout(function(){
			$scope.timePaster = true;
			console.log("demorou...")
			Utils.hide()
	   }, 1200);
   }



    ionicMaterialInk.displayEffect();
}; // fim da função
})(); // fim do documento
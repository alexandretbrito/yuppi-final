(function() {
'use_strict';

angular.module('APPlanner').controller('CalendasCtrl', CalendasCtrl);

function CalendasCtrl($scope, $rootScope, $localStorage, accessFactory, $firebaseArray, $location, Utils, $timeout){

	$rootScope.noMeetings = true;

	$scope.$on("$ionicView.enter", function(event, data){
		console.log("cheguei chegando")
		Utils.spinnerCalendar();
		if($localStorage.eventos){
			//$scope.loadEvents();
			$scope.sharedEvts()
		}else{
			retrievEvt($localStorage.usuario.uid);
			console.log("vamos ver se agora carrega");
		};
	});
		if($localStorage.usuario){
			if(!$localStorage.eventos || $localStorage.eventos== {}){	
				retrievEvt($localStorage.usuario.uid);
			}else{
				$rootScope.allEvents = $localStorage.eventos;
				console.log("funciona daqui sempre?");
			}
		}else{
			$location.path("/login");
			$rootScope.hider = false;
		}

		function retrievEvt(uid){
			console.log("retrievEvt")
			console.log(uid);
			var evPath = accessFactory.pegaEventList(uid);
			evPath.once('value').then(function(snapshot){
				console.log(snapshot.val())
				$localStorage.eventos = snapshot.val();
				$rootScope.allEvents = $localStorage.eventos;
				$scope.sharedEvts();
			});
		};

////////////////////////////////////////////
////////////////////////////////////////////
		function bringEvents(){
			console.log("bring events")
			console.log($localStorage.eventos)
			var i=0
	        var bjec = {};
			var events = [];
	        bjec = $localStorage.eventos;
	        Utils.hide();
	        angular.forEach(bjec, function(shops, key){
				var objent = {}
				objent.tipo = "evento";
				objent.chamamento = "Evento";
	        	objent.title = shops.name;
	        	objent.allDay = true;
	        	//
	        	var evtdate = shops.evtDay
	        	var brokeDate = [];
	        	brokeDate = evtdate.split("/");
	        	//
	        	objent.startTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
	        	objent.endTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
	        	objent.ID = shops.ID;
	        	objent.evtHour = shops.evtHour;
	        	objent.compareTime = datetoalign(brokeDate, objent.evtHour);
	        	events[i] = objent;
	        	//
	        		//datetoalign(brokeDate, objent.evtHour);
	        	//
            i++;
	        });
	        $rootScope.noMeetings = true;
	        //$rootScope.indicator = "Eventos em ";
	        $rootScope.labelDont = "Sem Eventos";
	        return events;
		}

		function bringMeetings(){
			console.log("bring meetings")
			console.log($localStorage.eventos)
			var i=0
	        var bjec = {};
			var meetings = [];
			var meets = {};
	        bjec = $localStorage.eventos;
	        Utils.hide();
	        angular.forEach(bjec, function(shops, key){
	        	if(shops.reunioes){
	        		var meets = {};
	        		meets = shops.reunioes;
	        		angular.forEach(meets, function(conv, key2){
	        	 if(conv.done == false){
	        			var objent = {};
						console.log("tem reunião em " + conv.dia);
						objent.tipo = "reuniao";
						objent.chamamento = "Reuniâo";
			        	objent.title = conv.local;
			        	objent.allDay = true;
			        	var evtdate = conv.dia;
			        	var brokeDate = [];
			        	brokeDate = evtdate.split("/")
			        	//
			        	objent.startTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
			        	objent.endTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
			        	objent.ID = shops.ID;
			        	objent.evtHour = conv.hora;
			        	meetings.push(objent);
            	 }
	            	});
            	}
            	i++;
	        });
	        $rootScope.noMeetings = false;
	        $rootScope.labelDont = "Sem Reuniões";
	        console.log(meetings)
	        return meetings;
		};


		$scope.sharedEvts = function(){
			var meetArr = [];
			var evtArr = [];
			var tuttiArrs = [];
			var evtArr = bringEvents();
			var meetArr = bringMeetings();
			tuttiArrs = evtArr.concat(meetArr);
			$scope.calendar.eventSource = tuttiArrs;
		}


		$scope.seeEvt = function(){
			$scope.calendar.eventSource = [];	
           $scope.calendar.eventSource = bringEvents();
           console.log("carregando eventos...")
           console.log($scope.calendar.eventSource)
		}

		$scope.seeMeet = function(){
			$scope.calendar.eventSource = [];
           $scope.calendar.eventSource = bringMeetings();
           console.log("carregando reuniões...")
           console.log($scope.calendar.eventSource)			
		}

        $scope.onViewTitleChanged = function (title) {
            $scope.viewTitle = title;
        };

        function datetoalign(dateArr, hora){
			console.log("aqui começa a zoeira da ordenação");
			var supertime = dateArr[2]+"-"+dateArr[1]+"-"+dateArr[0]+"T"+hora+":00";
			var numTime = new Date(supertime).getTime();
			console.log(new Date(numTime))
			return numTime;
        }

        $scope.calendar = {};
        $scope.changeMode = function (mode) {
            $scope.calendar.mode = mode;
        };

        $scope.today = function () {
            $scope.calendar.currentDate = new Date();
        };

        $scope.isToday = function () {
            var today = new Date(),
                currentCalendarDate = new Date($scope.calendar.currentDate);

            today.setHours(0, 0, 0, 0);
            currentCalendarDate.setHours(0, 0, 0, 0);
            return today.getTime() === currentCalendarDate.getTime();
        };	

		$scope.onEventSelected = function (event) {
            console.log()
            $rootScope.eventoDaVez = $rootScope.allEvents[event.ID]
            console.log($rootScope.eventoDaVez)
			$location.path('app/eventounico');
            $rootScope.asInvited = false;
        };

        $scope.onTimeSelected = function (selectedTime, events, disabled) {
            console.log('Selected time: ' + selectedTime + ', hasEvents: ' + (events !== undefined && events.length !== 0) + ', disabled: ' + disabled);

			$timeout(function(){	
            if(!events){
            	console.log("Não tem Eventos. Coloco um?")
			}else{
				console.log(events[0].tipo)
				if (events[0].tipo == "reuniao") {
					$rootScope.indicator = "Reunião agendada em ";
				} else {
					$rootScope.indicator = "Evento em ";
				}
			}
            }, 600);
        };

		$scope.loadEvents = function () {
	        $scope.seeEvt();
        };

}; // fim da função
})(); // fim do documento
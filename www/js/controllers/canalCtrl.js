(function() {
    'use_strict';
  
    angular.module('APPlanner').controller('canalCtrl', canalCtrl);
  
    function canalCtrl($scope, $http, $ionicLoading, $ionicPopup) {
        $scope.sendMail = function(nome, intento) {
            var theMail = "thaismontechiari@gmail.com"
	    	var send_add = "contato@yuppi.app";
			var mailgunUrl = "sandbox896ab695be0045baaa1f7acf17ed4b88.mailgun.org";
			var mailgunApiKey = window.btoa("api:key-2a6fbdd3571c5609e43e4f0c0f576c69");
	        $http(
	            {
	                "method": "POST",
	                "url": "https://api.mailgun.net/v3/" + mailgunUrl + "/messages",
	                "headers": {
	                    "Content-Type": "application/x-www-form-urlencoded",
	                    "Authorization": "Basic " + mailgunApiKey
	                },
	                data: "from=" + theMail + "&to=" + theMail + "&subject=" + "Email de contato do aplicativo Yuppi de "+ nome + "&text=" + intento}
	        ).then(function(success) {
	            console.log("SUCCESS " + JSON.stringify(success));
	            $ionicPopup.alert({
                  title: 'Sucesso!',
			      template: 'Seu email foi enviado. Obrigado.'
			    });
	        }, function(error) {
	            console.log("ERROR " + JSON.stringify(error));
	            $ionicPopup.alert({
                  title: 'Erro',
			      template: 'Seu email não foi enviado. Verifique sua conexão.'
			    });
	        });
	    }
    }; //fim da função
  })(); // fim do documento
(function() {
'use_strict';

angular.module('APPlanner').controller('GalleryCtrl', GalleryCtrl);

function GalleryCtrl($scope, $rootScope, $ionicPlatform, ionicMaterialInk, $ionicPopup, $ionicModal, $firebaseArray, $localStorage, $cordovaNetwork, planFactory, accessFactory, Utils, $location, $timeout, $cordovaImagePicker, $cordovaFile, $ionicSlideBoxDelegate, $cordovaCamera) {
	// body...
    $scope.images = [];
    var permissions = cordova.plugins.permissions;
    $scope.$on("$ionicView.enter", function(event, data){
       // handle event
       $scope.usuarioAtivo = $rootScope.usuarioAtivo;
       $scope.changedPhoto = false;
       $scope.saved = false;
       $scope.imaginarium = [];
       startGallery();

    });

    document.addEventListener("deviceready", function () {
     $scope.connec = $cordovaNetwork.isOnline();
     console.log($scope.connec)
     prepare_perm();
    }); 

    function prepare_perm() {
      // body...
        permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function( status ){
        if ( status.hasPermission ) {
          console.log("Yes :D ");
        }
        else {
          console.log("No :( ");
          permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, success, error);
           
          function error() {
            console.log('Camera permission is not turned on');
          }
           
          function success( status ) {
            if( !status.hasPermission ) error();
          }          
        }
         });

    };



	$scope.InsertMorePhotos = function(){
		console.log("InsertMorePhotos")
        var fileName, path;

        var options = {
              maximumImagesCount: 1, // Max number of selected images, I'm using only one for this example
              width:800,
              quality: 80            // Higher is better
        };
        console.log("aqui foi")


        $cordovaImagePicker.getPictures(options).then(function(results){
              console.log(results)
              console.log('Image URI: ' + results[0]);   // Print image URI
              if(results[0] != undefined){
                fileName = results[0].replace(/^.*[\\\/]/, '');
              }  
              if ($ionicPlatform.is("android")) {
                path = cordova.file.cacheDirectory;
              } else {
                path = cordova.file.tempDirectory;
              }

              return $cordovaFile.readAsArrayBuffer(path, fileName);
          }).then(function(success){
              var imageBlob = new Blob([success], { type: "image/jpeg" });
              console.log(imageBlob);
              $scope.saveImg(imageBlob, fileName);
          });
        /*
        window.imagePicker.getPictures(
          function(results) {
              //            
              console.log("foi mais adiante")
              console.log('Image URI: ' + results[0]);   // Print image URI
              if(results[0] != undefined){
                fileName = results[0].replace(/^.*[\\\/]/, '');
              }  
              if ($ionicPlatform.is("android")) {
                path = cordova.file.cacheDirectory;
              } else {
                path = cordova.file.tempDirectory;
              }

              return $cordovaFile.readAsArrayBuffer(path, fileName);
          }, function (error) {

          }, {
              maximumImagesCount: 1, // Max number of selected images, I'm using only one for this example
              width:800,
              quality: 80            // Higher is better
          }

        
        ); 
          */ 


        };





        function picSuccess(imageURI) {
            var image = document.getElementById('myImage');
            image.src = imageURI;
            console.log(image)
        }

        function picFail(message) {
            alert('Failed because: ' + message);
        }

        $scope.saveImg = function(blober, filer){
        
        var storageRef = firebase.storage().ref();

        // pass in the _filename, and save the _imageBlob
        var uploadTask = storageRef.child(filer).put(blober);

        uploadTask.on('state_changed', function (snapshot) {
          // Observe state change events such as progress, pause, and resume
          // See below for more detail
        }, function (error) {
          // Handle unsuccessful uploads, alert with error message
          alert(error.message)
        }, function () {
          // Handle successful uploads on complete
          var downloadURL = uploadTask.snapshot.downloadURL;
          $scope.nameURL = uploadTask.snapshot.a.fullPath;
          $scope.theFoto = downloadURL;
          $scope.$apply();
          console.log("//////////////////////")
          console.log($scope.nameURL)
          $scope.changedPhoto = true;
          saveDrone();
        });        

        };

        function saveDrone() {
        	var uniImage = {
            ref: $scope.nameURL,
        		src: $scope.theFoto
        	}
          uniImage = angular.copy(uniImage)
			     $scope.imaginarium.push(uniImage);
        	grave_me()
        }

        function grave_me(){
          console.log("gravar para "+ $rootScope.eventoDaVez.ID)
	        var bje = {}
	        angular.forEach($scope.imaginarium, function(todes, keyo){
	            bje[keyo] = $scope.imaginarium[keyo];
	            bje = angular.copy(bje);
	            console.log(bje)
	        });
	        if($scope.connec){
	        	Utils.show();
	            console.log("vou gravar na internet")
	              planFactory.insertGallery($rootScope.eventoDaVez.ID, bje);     
	        }else{
	            console.log("não vou gravar na internet")
	            ionicToast.show('Não há conexão com a internet.', 'middle', false, 3000);
	        }
        }


		$rootScope.$on("galleryDone", function (event) {
			Utils.hide();
          	Utils.alertshow('Sucesso', 'Galeria de Imagens atualizada.');
          mountGal();
      });

		function startGallery() {
			$timeout(function(){
        console.log("start gallery");
				mountGal();
			}, 600);
		}

		function mountGal() {
			$scope.imaginarium = [];
			var minhasGal = planFactory.retrieveGallery($rootScope.eventoDaVez.ID);
			minhasGal.once('value').then(function(snapshot){
				result = snapshot.val();
				angular.forEach(result, function(value, kay){
					$scope.imaginarium[kay] = value;
					console.log(value);
          $scope.$apply();
				});
			})
		}


    $scope.delImg = function(indego){
       var confirmPopup = $ionicPopup.confirm({
         title: 'Deletar',
         template: 'Tem certeza que quer retirar a imagem da galeria?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() { 
             console.log('You are sure');
              $scope.subDelete(indego)        
           }
         }
        ]
       })
      }

      $scope.subDelete = function(indego2){
      console.log(indego2)
      var storageRef = firebase.storage().ref();
      var fotRef = storageRef.child($scope.imaginarium[indego2].ref);
      fotRef.delete().then(function(){
        console.log("imagem deletada")
      }).catch(function(error){
        console.log(error);
      })
      $scope.imaginarium.splice(indego2, 1);
      grave_me()
    }
}

})(); // fim do documento
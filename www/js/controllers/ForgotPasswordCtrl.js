(function() {
'use_strict';

angular.module('APPlanner').controller('ForgotPasswordCtrl', ForgotPasswordCtrl); 
function ForgotPasswordCtrl($scope, $state, FireAuth) {
  //$scope.user;
  $scope.recoverPassword = function(){
    //$state.go('app.entrada');
    console.log($scope.user.email);
    FireAuth.resetPass($scope.user.email);
  };

  $scope.user = {};
}
})(); // fim do documento
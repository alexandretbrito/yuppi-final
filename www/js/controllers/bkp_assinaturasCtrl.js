(function() {
  'use_strict';

  angular.module('APPlanner').controller('assinaturasCtrl', assinaturasCtrl);

  function assinaturasCtrl($scope, $rootScope, $localStorage, $filter, $sessionStorage,$ionicModal, $ionicPopover, $timeout, $location, Utils, $cordovaNetwork, PagseguroService) {

  	$scope.pagante = $rootScope.usuarioAtivo.pagante;
    $scope.mensal = false;
    $scope.semestral = false;
    $scope.anual = false;
    $scope.card = {};
    $scope.info = {}

        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
        });


    $scope.$on('$ionicView.leave', function(){
        //Utils.alertshow('Saindo', 'Deixando o prédio!.');
        console.log('Deixando o prédio!.');
        $scope.card = {};
        $scope.mensal = false;
   		$scope.semestral = false;
    	$scope.anual = false;
    	delete $sessionStorage.dados;
      delete $sessionStorage.session;
    });

    $scope.$on("$ionicView.enter", function(event, data){
         if(!$scope.connec){
          Utils.alertshow("Aviso", "No Momento não há conexão à internet.")
        }else{
          if($sessionStorage.session == undefined){
            console.log('criar o hash/token para pagamento');
            PagseguroService.startSession();
          }else{
            console.log('a sessão já foi aberta');
            console.log($sessionStorage.session)
          }
        }
    });


	$rootScope.previousState;
	$rootScope.currentState;
	$rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
	    $rootScope.previousState = from.name;
	    $rootScope.currentState = to.name;
	    console.log('Previous state:'+$rootScope.previousState)
	    console.log('Current state:'+$rootScope.currentState)
	});

    console.log("Assinatura ativa? "+ $scope.pagante);

  $scope.getPlano = function(plan){
  	if(plan == 'mensal'){
	    $scope.mensal = true;
	    $scope.semestral = false;
	    $scope.anual = false;
 		$scope.plano = plan;
  	}
  	if(plan == 'semestral'){
	    $scope.mensal = false;
	    $scope.semestral = true;
	    $scope.anual = false;
 		$scope.plano = plan;
  	}
  	if(plan == 'anual'){
	    $scope.mensal = false;
	    $scope.semestral = false;
	    $scope.anual = true;
 		$scope.plano = plan;
  	}
  }

  $scope.masterPlan = function() {
   	if($scope.plano == undefined){
        Utils.alertshow("Aviso", "Você deve escolher um plano.")  		
  	}else{ 	
	  	var session = {}; 
	  	console.log("plano");
	  	console.log($scope.plano);
	  	if($scope.plano == 'mensal'){
	  		session.season = 'MONTHLY';
	  		session.amount = '69.90';
	  	}
	  	if($scope.plano == 'semestral'){
	  		session.season = 'SEMESTER';
	  		session.amount = '359.40';	  	
	  	}
	  	if($scope.plano == 'anual'){
	  		session.season = 'YEARLY';
	  		session.amount = '598.80';	  	
	  	}
	  	$sessionStorage.dados = session;
	  	console.log($sessionStorage.dados);
	  	$scope.openModal(1)
      PagseguroService.makeSession();

  	}
  }

	 $ionicModal.fromTemplateUrl('templates/auxiliar/pagseg_creditcard.html', {
	      id: '1',
	      scope: $scope,
	      animation: 'slide-in-up'
	 }).then(function(modal) {
	      $scope.modal1 = modal;
	 });

   $ionicModal.fromTemplateUrl('templates/auxiliar/pagseg_data.html', {
        id: '2',
        scope: $scope,
        animation: 'slide-in-up'
   }).then(function(modal) {
        $scope.modal2 = modal;
   });    

	$ionicModal.fromTemplateUrl('templates/auxiliar/pagseg_resultsign.html', {
	      id: '3',
	      scope: $scope,
	      animation: 'slide-in-up'
	 }).then(function(modal) {
	      $scope.modal3 = modal;
	 });


   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }
      if (index == 2){
       $scope.modal2.show();
      }
      if (index == 3){
       $scope.modal3.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }
      if (index == 2){
       $scope.modal2.hide();
      }
      if (index == 3){
       $scope.modal3.hide();
      }
   };

   $scope.cancelSign = function(indice) {
		$scope.resetCard(indice);   		
   };

   $scope.resetCard = function(indice){
      $scope.card = {};
      $scope.mensal = false;
      $scope.semestral = false;
      $scope.anual = false;
    $scope.closeModal(indice);
   };

   $scope.saveCard = function(indice){
      $sessionStorage.dados.cartao = $scope.card;
      console.log($sessionStorage.dados);
      $scope.closeModal(1);
      PagseguroService.secureCard($sessionStorage.dados.cartao);
   };

   $scope.eraseData = function(){

   };

   $rootScope.$on('successCardToken', function(event){
        console.log("dados e token salvos!");
      $scope.closeModal(1);
      $timeout(function(){
        $scope.openModal(2);
      }, 500);
   })

   $rootScope.$on('failCardToken', function(event){
        $scope.closeModal(1);
        Utils.alertshow("Operação nao autorizada", "Dados do cartão incorretos.");    
   })

  $scope.saveBuyer = function(indice){
    //preparando os ddos para serem consumidos
    //dados de plano
      $scope.info.reference = $localStorage.usuario.uid;
    //CPF e País
    $scope.info.sender.documents.type = "CPF";
    $scope.info.sender.address.country= "BRA";
    $scope.info.sender.ip = "192.168.0.1";
    // pagamento
    //aniversario
    var aniversario = $filter('date')($scope.info.paymentMethod.creditCard.holder.birthday, 'dd/MM/yyyy');

    $scope.info.paymentMethod.creditCard.holder.birthday = String(aniversario);
    //dados cartao
    $scope.info.paymentMethod.type = 'CREDITCARD';
    $scope.info.paymentMethod.creditCard.token = $sessionStorage.dados.cartao.token;
    $scope.info.paymentMethod.creditCard.holder.name = $sessionStorage.dados.cartao.nomecard;
    $scope.info.paymentMethod.creditCard.billingAddress = $scope.info.sender.address;
    $scope.info.paymentMethod.creditCard.phone = $scope.info.sender.phone;

    PagseguroService.startSell($scope.info);
    $scope.closeModal(2);
  }

  $scope.resetBuyer = function(indice){
    $scope.info = {};
    $scope.closeModal(2);
  }

  }; //fim da função
})(); // fim do documento

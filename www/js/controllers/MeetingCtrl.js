(function() {
'use_strict';

angular.module('APPlanner').controller('MeetingCtrl', MeetingCtrl);

function MeetingCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, Utils, $ionicPopup, $filter){
   $scope.$parent.showHeader();

    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;
    $scope.changedDate = false;

    $scope.listCheck = [];
    if($rootScope.asInvited){
      if($localStorage.colabs[$scope.theEvent].reunioes){
            var bjec = {};
            bjec = $localStorage.colabs[$scope.theEvent].reunioes;
            angular.forEach(bjec, function(tods, key){

                $scope.listCheck[key] = angular.copy(tods);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)

        }else{
            console.log("segue vazio")
        }
    }else{
      if($localStorage.eventos[$scope.theEvent].reunioes){
          var bjec = {};
          bjec = $localStorage.eventos[$scope.theEvent].reunioes;
          angular.forEach(bjec, function(tods, key){

              $scope.listCheck[key] = angular.copy(tods);
              //console.log(bjec)
          });
          console.log("vai")
          console.log(bjec)

      }else{
          console.log("segue vazio")
      }
    }

        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
        }); 

        $scope.showTaskPrompt = function() {
            var newTask = {
                local: '',
                done: false
            };  

            var saveDay = new Date();
            saveDay.setSeconds(0);
            saveDay.setMilliseconds(0);

		    $scope.itemDate = {
		     	value: saveDay
		   	};

		    $scope.itemTime = {
		     	value: saveDay,
		   	};			   	

		   	console.log($scope.itemDate)
		   	console.log($scope.itemTime)

            $scope.newTask = newTask;
            $scope.openModal(1);
        };


 		$scope.primeHour = function(){
			var saveDay = new Date();
			$scope.itemTime = {
	    		value: saveDay
	    	}; 
	
 		}

 		$scope.seeChange01 = function(){
 			$scope.changedDate = true;
 		}

//////////////////////////////////////////////////////////

function engMonth(mes){
var abrev = "";
if(mes === "01"){
	abrev = "Jan"; 
}else if(mes === "02"){
	abrev = "Feb";	
}else if(mes === "03"){
	abrev = "Mar";	
}else if(mes === "04"){
	abrev = "Apr";	
}else if(mes === "05"){
	abrev = "May";	
}else if(mes === "06"){
	abrev = "Jun";	
}else if(mes === "07"){
	abrev = "Jul";	
}else if(mes === "08"){
	abrev = "Aug";	
}else if(mes === "09"){
	abrev = "Sep";	
}else if(mes === "10"){
	abrev = "Oct";	
}else if(mes === "11"){
	abrev = "Nov";	
}else if(mes === "12"){
	abrev = "Dec";	
}

return abrev;

}






        $scope.saveTask = function(indice) {
          if($scope.newTask.local != ''){
              console.log("gravou");
              $scope.newTask.hora = $filter('date')($scope.itemTime.value, 'HH:mm');
              $scope.newTask.dia = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');

				var shaca = $scope.newTask.dia;
				var utcMes = shaca.split("/");
				var diaCal = utcMes[0];
				var anoCal = utcMes[2];
				var mesCal =  utcMes[1];
				var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;

				$scope.newTask.calId = calId;

              $scope.listCheck.push($scope.newTask);
              $scope.closeModal(indice);
              $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o local da reunião")
          }
        };          

        $scope.cancelTask = function(indice) {
            $scope.closeModal(indice);
	 		$scope.itemTime = null;
			$scope.itemDate = null;           
        };

///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
    	state.done = true;
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].reunioes = $scope.listCheck;
          $scope.insertIntoObj();
        }else{
           $localStorage.eventos[$scope.theEvent].reunioes = $scope.listCheck;
          $scope.insertIntoObj();         
        }      

    }

    $scope.removeMe = function(itim){
    	console.log(itim.local)
    	//$scope.listCheck.splice(indi, 1)
    	var aquele = $scope.listCheck.indexOf(itim)
    	console.log(aquele)
    	$scope.listCheck.splice(aquele, 1)
        $scope.insertIntoObj();
    }

  $scope.delEvento = function(){
     // A confirm dialog
     var confirmPopup = $ionicPopup.confirm({
       title: 'Deletar',
       template: 'Tem certeza que quer deletar?',
       cssClass: 'my-popup',
       buttons: [
         { text: 'Não',
            type: 'button-assertive myred',
          },
       {
         text: 'Sim',
         type: 'button-positive-900',
         onTap: function() { 
           console.log('You are sure');
         $scope.removefromUpdate(2)        
         }
       }
      ]
     })

     confirmPopup.then(function(res) {
      console.log(res)
       if(res) {

       } else {
         console.log('You are not sure');
       }
     });
  };     

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
        $scope.listCheck.splice($scope.indiceMod, 1)
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done == false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
    };  

    $scope.grave_me = function(objeto) {
        // body...
        if($scope.connec){
            console.log("vou gravar na internet")
            console.log($localStorage.eventos)
            if($rootScope.asInvited){
              planFactory.insertMeeting($rootScope.eventoDaVez.uid, $scope.theEvent, objeto) 
            }else{
               planFactory.insertMeeting($localStorage.usuario.uid, $scope.theEvent, objeto)           
            }            
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }   


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(todes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        if($rootScope.asInvited){
           $localStorage.colabs[$scope.theEvent].reunioes = bje;
        }else{
          $localStorage.eventos[$scope.theEvent].reunioes = bje;         
        }

        //ionicToast.show('Sucesso', 'middle', false, 1000);
        $scope.grave_me(bje);        
    };


    $scope.editTask = function(index, obito){
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.backupItem = {};
        $scope.modifItem = obito;       
        $scope.backupItem = $scope.modifItem;
        console.log("entrou")
        console.log($scope.modifItem)        



		var timeSeed = $scope.modifItem.hora;
		var arr = timeSeed.split(':');
		var hr = arr[0];
		var mn = arr[1];
		var saveData = new Date(0);
		saveData.setHours(hr);
		saveData.setMinutes(mn);
		$scope.itemTime = {
	    	value: saveData
		};
		console.log($scope.itemTime.value +" de horas") 

		var diaD = $scope.modifItem.dia;
		var diaArr = diaD.toString().split('/');
		var finalCountDown = new Date(diaArr[2], diaArr[1], diaArr[0]);
	    $scope.itemDate = {
	     	value: finalCountDown
	   	}; 

        $scope.openModal(2)

     };   

    $scope.notUpdateTask = function(indice, item){
        console.log("saiu sem salvar");
        $scope.modifItem.local = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        console.log($scope.backupItem);
        console.log($scope.modifItem);
        delete $scope.backupItem;
        delete $scope.modifItem;
        $scope.closeModal(indice);
    }

    $scope.updateTask = function(indice) {
        console.log("saiu")
        console.log($scope.modifItem)
		$scope.modifItem.hora = $filter('date')($scope.itemTime.value, 'HH:mm');
		$scope.modifItem.dia = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');

				var shaca = $scope.modifItem.dia;
				var utcMes = shaca.split("/");
				var diaCal = utcMes[0];
				var anoCal = utcMes[2];
				var mesCal =  utcMes[1];
				var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;

				$scope.modifItem.calId = calId;

        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };


     $ionicModal.fromTemplateUrl('templates/auxiliar/inserereuniao.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });
    
    $ionicModal.fromTemplateUrl('templates/auxiliar/modificareuniao.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }else{
       $scope.modal2.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }else{
       $scope.modal2.hide();
      }
   
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });   


}; // fim da função
})(); // fim do documento
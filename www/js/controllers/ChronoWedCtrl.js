(function() {
'use_strict';

angular.module('APPlanner').controller('ChronoWedCtrl', ChronoWedCtrl);

function ChronoWedCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, $filter, $timeout, Utils, $ionicPopup){

   $scope.$parent.showHeader();

   $scope.$on("$ionicView.enter", function(event, data){
        console.log("começo")
        if($rootScope.asInvited){
            console.log($localStorage.colabs[$scope.theEvent].chron);
                 $timeout(function(){
                     if($localStorage.colabs[$scope.theEvent].chron == undefined){
                        console.log("não tem")
                        $scope.firstContact();
                    }
                   else{
                        console.log("tem")
                        $scope.secondContact();
                    }
                 }, 1000);
        }else{
            console.log($localStorage.eventos[$scope.theEvent].chron);
             $timeout(function(){
                 if($localStorage.eventos[$scope.theEvent].chron == undefined){
                    console.log("não tem")
                    $scope.firstContact();
                }
               else{
                    console.log("tem")
                    $scope.secondContact();
                }
             }, 1000);          
        }
   });

    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;

    $scope.bestMan = false;
    $scope.plusBest = false;
    $scope.justBest = false;
    $scope.casale = {};
    $scope.casaleIndex = 0;
    $scope.allTheBest = {};
    $scope.showTheBest = [];

    $scope.bestSong = false;
    $scope.justSong = false; 
    $scope.plusSong = false;
    $scope.parade = {};
    $scope.paradeIndex = 0;
    $scope.allTheSongs= {};
    $scope.showTheSongs = [];

    $scope.retroMes = true;
    $scope.indexed = 0;

    $scope.listCheck = [];
    $scope.actualTab = "pre";
    $scope.restamMeses = 0;

    $scope.timeBefore = [];

    $scope.listPreWedding = []; //lista de Pre Wedding
    $scope.listCerimonia = []; //Lista da Cerimônia
    $scope.listFesta = [];//Lista da Festa

    document.addEventListener("deviceready", function () {
     $scope.connec = $cordovaNetwork.isOnline()
     console.log($scope.connec)
     console.log("eu sou um casamento")
    }); 

        $scope.showTaskPrompt = function() {
          if($scope.actualTab === "pre"){
            var newShop = {
                item: '',
                time: 0,
                done: false
            };
            $scope.newShop = newShop;
          }else{
            if($scope.bestMan){
              var newShop = {
                  item: 'Entrada dos Padrinhos',
                  time: "18:00",
                  bm: true,
                  done: false
              };
              $scope.casale = {};
            }else{
              var newShop = {
                  item: '',
                  time: "18:00",
                  done: false
              };            
            }
              $scope.newShop = newShop;
              var chop = $scope.newShop.time;
              var arr = chop.split(':');
              var hr = arr[0];
              var mn = arr[1];
              var saveData = new Date(0);
              saveData.setHours(hr);
              saveData.setMinutes(mn);
              $scope.itemTime = {
                  value: saveData
              }; 
          }


            //console.log($scope.itemTime);

            $scope.theBitsi = {};
            //$scope.hora = $scope.newShop.time;
            $scope.theBitsi.meses = $scope.newShop.time;
            $scope.theBitsi.values = $scope.timeBefore;
   

            $scope.openModal(1);
        };

        $scope.bestManPrompt = function(){
          console.log("Melhor Homem")
          $scope.bestMan = true;
          $scope.showTaskPrompt()
        }

        $scope.saveTask = function(indice) {
          if($scope.newShop.item != ''){          
              console.log("gravou")
              if($scope.actualTab === "pre"){
                console.log($scope.theBitsi.meses +" meses faltantes")
                $scope.newShop.time = $scope.theBitsi.meses;
                $scope.listPreWedding.push($scope.newShop);              
              }
              if($scope.actualTab === "ceremony"){
                var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
                $scope.newShop.time = datafinal;
                console.log($scope.newShop)
                $scope.listCerimonia.push($scope.newShop);                  
              }
              if($scope.actualTab === "party"){
                var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
                $scope.newShop.time = datafinal;
                console.log($scope.newShop)
                $scope.listFesta.push($scope.newShop);
              }            
              $scope.closeModal(indice);
              $scope.insertIntoObj();
           }else{
              Utils.alertshow("Erro", "Indique o nome do item")
          }           
        };

        $scope.cancelTask = function(indice) {
            $scope.closeModal(indice);
            if($scope.bestMan == true){
              $scope.bestMan = false;
              $scope.plusBest = false;
              $scope.casale = {};
            }
        };

        $scope.firstContact = function(){
          console.log("------------------------------------------------------------")
          console.log("                      PRIMEIRO CONTATO                      ")
          console.log("------------------------------------------------------------")
          // Primeiro o PreWeeding
          var objPreWedding = {};
          objPreWedding = preWed;
          //objPreWedding = extraData.getPreWed();
          var objWedding = {};
          objWedding = aCerimonia;
          //objWedding = extraData.getCeremony();
          var objParty = {};
          objParty = aFesta;
          //objParty = extraData.getParty();
          var all = {};
          all.pre = objPreWedding;
          all.wedding = objWedding;
          all.party = objParty;

          angular.forEach(all.pre, function(shops, key){
              $scope.listPreWedding[key] = angular.copy(shops);
              console.log($scope.listPreWedding)
          });
          //depois a cerimônia
          angular.forEach(all.wedding, function(shops, key){
              shops.time = "18:00"
              $scope.listCerimonia[key] = angular.copy(shops);
              console.log($scope.listCerimonia)
          });
          //por último a festa
          angular.forEach(all.party, function(shops, key){
              shops.time = "18:00";
              console.log("---------------------------")
              console.log(shops)
              $scope.listFesta[key] = angular.copy(shops);
              $scope.listFesta[key].time = "18:00";
              
          });
          // gravar no $localStorage
          if($rootScope.asInvited){
            $localStorage.colabs[$scope.theEvent].chron = all;
          }else{
            $localStorage.eventos[$scope.theEvent].chron = all;
          }
          //gravar no Firebase        
          all = angular.copy(all)
          $scope.gravacao(all);
        };


        $scope.secondContact = function(){
          console.log("------------------------------------------------------------")
          console.log("                      SEGUNDO CONTATO                       ")
          console.log("------------------------------------------------------------")
          // Primeiro o PreWeeding
          var all = {};
          if($rootScope.asInvited){
            all = $localStorage.colabs[$scope.theEvent].chron;
          }else{
            all = $localStorage.eventos[$scope.theEvent].chron;
          }

          angular.forEach(all.pre, function(shops, key){
              $scope.listPreWedding[key] = angular.copy(shops);
              //console.log($scope.listPreWedding)
          });
          //depois a cerimônia
          angular.forEach(all.wedding, function(shops, key){
              $scope.listCerimonia[key] = angular.copy(shops);
              //console.log($scope.listCerimonia)
              if($scope.listCerimonia[key].padrinhos && $scope.listCerimonia[key].bm){
                console.log("PADRINHOS!!!!!")
                $scope.viewBest(key);
              }
              if($scope.listCerimonia[key].playlist && $scope.listCerimonia[key].mus){
                console.log("PLAYLIST!!!!!")
                $scope.viewSong(key);
              }

              //INSERIR AQUI O RETORNO DOS PADRINHOS

          });
          //por último a festa
          angular.forEach(all.party, function(shops, key){
              $scope.listFesta[key] = angular.copy(shops);
              //console.log($scope.listFesta)
          });
          // gravar no $localStorage
          if($rootScope.asInvited){
              $localStorage.colabs[$scope.theEvent].chron = all;
          }else{
              $localStorage.eventos[$scope.theEvent].chron = all;
          }
          $scope.gravacao(all);
        

        }


  $scope.tabSelected = function(tab) {
    console.log(tab + ' Tab Selected');
    $scope.actualTab = tab;
    if($scope.actualTab === "pre"){

    }
    if($scope.actualTab === "ceremony"){
          
    }
    if($scope.actualTab === "party"){
          
    }
  };



///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        if($rootScope.asInvited){
            $localStorage.colabs[$scope.theEvent].chron = $scope.listCheck;
        }else{
            $localStorage.eventos[$scope.theEvent].chron = $scope.listCheck;
        }
        $scope.insertIntoObj();
    }

    $scope.removeMe = function(itim){
        console.log(itim)
      if($scope.actualTab === "pre"){
          var aquele = $scope.listPreWedding.indexOf(itim)
          console.log(aquele)
          $scope.listPreWedding.splice(aquele, 1)
      }
      if($scope.actualTab === "ceremony"){
          var aquele = $scope.listCerimonia.indexOf(itim)
          console.log(aquele)
          $scope.listCerimonia.splice(aquele, 1)          
      }
      if($scope.actualTab === "party"){
          var aquele = $scope.listFesta.indexOf(itim)
          console.log(aquele)
          $scope.listFesta.splice(aquele, 1)          
      }
        $scope.insertIntoObj();
    };

    $scope.delEvento = function(){
       // A confirm dialog
       var confirmPopup = $ionicPopup.confirm({
         title: 'Deletar',
         template: 'Tem certeza que quer deletar?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() { 
             console.log('You are sure');
           $scope.removefromUpdate(2)        
           }
         }
        ]
       })

       confirmPopup.then(function(res) {
        console.log(res)
         if(res) {

         } else {
           console.log('You are not sure');
         }
       });
    }; 

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
    if($scope.actualTab === "pre"){
        $scope.listPreWedding.splice($scope.indexed, 1)
    }
    if($scope.actualTab === "ceremony"){
        $scope.listCerimonia.splice($scope.indexed, 1)          
    }
    if($scope.actualTab === "party"){
        $scope.listFesta.splice($scope.indexed, 1)          
    }
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        console.log($scope.actualTab);
        var indexed = 0;
        if($scope.actualTab === "pre"){
          indexed = $scope.listPreWedding.indexOf(item);
          if($scope.listPreWedding[indexed].done === false){
              $scope.listPreWedding[indexed].done = true;
          }else{$scope.listPreWedding[indexed].done = false}
        }
        if($scope.actualTab === "ceremony"){
          indexed = $scope.listCerimonia.indexOf(item);
          if($scope.listCerimonia[indexed].done === false){
              $scope.listCerimonia[indexed].done = true;
          }else{$scope.listCerimonia[indexed].done = false}              
          }
        if($scope.actualTab === "party"){
          indexed = $scope.listFesta.indexOf(item);
          if($scope.listFesta[indexed].done === false){
              $scope.listFesta[indexed].done = true;
          }else{$scope.listFesta[indexed].done = false}              
        }
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
        console.log("fui acionada");
        if($scope.connec){
            console.log("vou gravar na internet");
            if($rootScope.asInvited){
              planFactory.insertCrono($rootScope.eventoDaVez.uid, $scope.theEvent, objeto)
            }else{
              planFactory.insertCrono($localStorage.usuario.uid, $scope.theEvent, objeto)
            }
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }

    $scope.gravacao = function(objeto){
        // body...
        console.log("fui acionada");
        if($scope.connec){
            console.log("vou gravar na internet")
            if($rootScope.asInvited){
              planFactory.insertCrono($rootScope.eventoDaVez.uid, $scope.theEvent, objeto)
            }else{
              planFactory.insertCrono($localStorage.usuario.uid, $scope.theEvent, objeto)
            }        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        if($scope.actualTab === "pre"){
          angular.forEach($scope.listPreWedding, function(shopes, keyo){
              bje[keyo] = $scope.listPreWedding[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
          if($rootScope.asInvited){
              $localStorage.colabs[$scope.theEvent].chron.pre = bje;
          }else{
              $localStorage.eventos[$scope.theEvent].chron.pre = bje;
          }
        }
        if($scope.actualTab === "ceremony"){
          angular.forEach($scope.listCerimonia, function(shopes, keyo){
              bje[keyo] = $scope.listCerimonia[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].chron.wedding = bje;
        }else{
          $localStorage.eventos[$scope.theEvent].chron.wedding = bje;
        }
          if($scope.bestMan == true){
              $scope.liner();
          }
          $scope.casale = {};          
        }
        if($scope.actualTab === "party"){
          angular.forEach($scope.listFesta, function(shopes, keyo){
              bje[keyo] = $scope.listFesta[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].chron.party = bje;               
        }else{
          $localStorage.eventos[$scope.theEvent].chron.party = bje;               
        }
        }
        var prox = {};
        if($rootScope.asInvited){
          prox = $localStorage.colabs[$scope.theEvent].chron;
        }else{
          prox = $localStorage.eventos[$scope.theEvent].chron;
        }
        $scope.grave_me(prox);        
    };


    $scope.editTask = function(index, obito, trueBests){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        $scope.modifItem = {};
        $scope.theItsi = {};
        $scope.justBest = trueBests;
        $scope.indiceMod = index;
        $scope.modifItem = obito;
        $scope.theItsi.meses = $scope.modifItem.time;
        $scope.theItsi.values = $scope.timeBefore;
        console.log("entrou")
        console.log($scope.modifItem)

        if($scope.actualTab === "pre"){
          $scope.indexed = $scope.listPreWedding.indexOf(obito);
        }
        if($scope.actualTab === "ceremony"){
              $scope.indexed = $scope.listCerimonia.indexOf(obito);
              $scope.retroMes = false;
              if($scope.modifItem.time == 0){
                  var datainicial = new Date(0);
                  $scope.itemTime = {
                   value: datainicial
                  };  
              }else{
                  var chop = $scope.modifItem.time;
                  var arr = chop.split(':');
                  var hr = arr[0];
                  var mn = arr[1];
                  var datainiciada = new Date(0);
                  datainiciada.setHours(hr);
                  datainiciada.setMinutes(mn);
                  $scope.itemTime = {
                      value: datainiciada
                  };          
              }; 
        }
        if($scope.actualTab === "party"){
           $scope.retroMes = false;
           $scope.indexed = $scope.listFesta.indexOf(obito);
           if($scope.modifItem.time == 0){
                  var datainicial = new Date(0);
                  $scope.itemTime = {
                   value: datainicial
                  };  
              }else{
                  var chop = $scope.modifItem.time;
                  var arr = chop.split(':');
                  var hr = arr[0];
                  var mn = arr[1];
                  var datainiciada = new Date(0);
                  datainiciada.setHours(hr);
                  datainiciada.setMinutes(mn);
                  $scope.itemTime = {
                      value: datainiciada
                  };          
              }; 
        }


        // ver padrinhos
        if($scope.modifItem.bm == true){
          $scope.bestMan = true;
          if($scope.modifItem.padrinhos){
           //aqui monta os padrinhos
           console.log("tem padrinhos");
           $scope.viewBest($scope.indexed);
          }
        }

        $scope.openModal(2);
    };

 $scope.changeEventType = function(sel){
    console.log("changeEventType")
    console.log(sel);
    $scope.restamMeses = sel;
  }      

    $scope.updateTask = function(indice) {
        console.log("saiu")
          if($scope.actualTab === "pre"){
            $scope.modifItem.time = $scope.restamMeses;
            $scope.listPreWedding[$scope.indexed] = $scope.modifItem;
          }
          if($scope.actualTab === "ceremony"){
            console.log($scope.itemTime.value)
            var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
            $scope.modifItem.time = datafinal;
            console.log($scope.modifItem)
            $scope.listCerimonia[$scope.indexed] = $scope.modifItem;
            console.log($scope.listCerimonia)              
          }
          if($scope.actualTab === "party"){
            console.log($scope.itemTime.value)
            var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
            $scope.modifItem.time = datafinal;
            console.log($scope.modifItem)
            $scope.listFesta[$scope.indexed] = $scope.modifItem;
            $scope.listFesta[$scope.indexed] = $scope.modifItem;               
          }
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };

     $ionicModal.fromTemplateUrl('templates/auxiliar/inserecronogramacasorio.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/modificacronogramacasorio.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });
    $ionicModal.fromTemplateUrl('templates/auxiliar/mostraPadrinhos.html', {
      id: '3',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal3 = modal;
   });
     $ionicModal.fromTemplateUrl('templates/auxiliar/alterapadrinhos.html', {
      id: '4',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal4 = modal;
   });
     $ionicModal.fromTemplateUrl('templates/auxiliar/mostraPlaylist.html', {
      id: '5',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal5 = modal;
   });
     $ionicModal.fromTemplateUrl('templates/auxiliar/alteraplaylist.html', {
      id: '6',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal6 = modal;
   });    

    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }
      if (index == 2){
       $scope.modal2.show();
      }
      if (index == 3){
       $scope.modal3.show();
      }
      if (index == 4){
       $scope.modal4.show();
      }
      if (index == 5){
       $scope.modal5.show();
      }
      if (index == 6){
       $scope.modal6.show();
      }          
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }
      if (index == 2){
       $scope.modal2.hide();
      }
      if (index == 3){
       $scope.modal3.hide();
      }
      if (index == 4){
       $scope.modal4.hide();
      }
      if (index == 5){
       $scope.modal5.hide();
      }
      if (index == 6){
       $scope.modal6.hide();
      }  
      if($scope.bestMan == true){
          $scope.bestMan = false;
      }
      if($scope.bestSong == true){
          $scope.bestSong = false;
      }
      $scope.justBest = false;
      $scope.casale = {}
      $scope.parade = {}
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
      $scope.modal3.remove();
      $scope.modal4.remove();
      $scope.modal5.remove();
      $scope.modal6.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });

///////////////////////////////////////////////////////////////////////////////////
//                          Parafernalha dos padrinhos                           //    
///////////////////////////////////////////////////////////////////////////////////
//    $scope.bestMan = false;
//    $scope.plusBest = false;
//    $scope.oCara = {};
//    $scope.allTheBest = {};
//    $scope.showTheBest = []; 



   //insere o padrinho - step 1
   $scope.putBest = function(){
      
      if(!$scope.casale.nomeDele || !$scope.casale.phoneDele || !$scope.casale.nomeDela || !$scope.casale.phoneDela || !$scope.casale.choice){
        Utils.alertshow("Erro", "Cadastro dos padrinhos incompleto")
      }else{
          $scope.plusBest = false;
         console.log($scope.casale);
         $scope.cadastraPadrinho();     
      }
   };

    //insere o padrinho - step 2
    $scope.cadastraPadrinho = function(){
        $scope.showTheBest.push($scope.casale);
        $scope.liner();
   }; 

   //insere o padrinho - step 3 | //deleta o padrinho - step 2
   $scope.liner = function(){
        var bester = {};
        angular.forEach($scope.showTheBest, function(shopes, keyo){
            bester[keyo] = $scope.showTheBest[keyo];
            bester = angular.copy(bester);
            console.log(bester);
        });
        $scope.listCerimonia[$scope.indexed].padrinhos = bester;
        $scope.insertIntoObj();    
   }

   //deleta o padrinho - step 1
   $scope.notSoBest = function(theGuy){
        var num = 0;
        var arr = $scope.listCerimonia;
        for(var i=0; i<arr.length; i++) {
            if(arr[i].padrinhos) {
                // achou!
                num = i
            }
        }

        $scope.indexed = num;

        var posit = $scope.showTheBest.indexOf(theGuy);
        $scope.showTheBest.splice(posit, 1)       
        $scope.liner();
   }

   //botão para mostrar o form de inserção do padrinho
   $scope.insertBest = function(){
      $scope.plusBest = true;
      $scope.casale = {};
   };



   $scope.improveTheBest = function(ictio){

    $scope.casaleIndex = $scope.showTheBest.indexOf(ictio);
    $scope.casale = {};
    $scope.casale = ictio;
    $scope.openModal(4);
  }

   $scope.correctBest = function(){
      var num = 0;
      var arr = $scope.listCerimonia;
      for(var i=0; i<arr.length; i++) {
          if(arr[i].padrinhos) {
              // achou!
              num = i
          }
      }
      $scope.indexed = num;

      $scope.showTheBest[$scope.casaleIndex] = $scope.casale;
      $scope.liner();
      $scope.closeModal(4);
   };

    //preenche o array de padrinhos
    $scope.viewBest = function(positron){
      var osCara = {};
      $scope.showTheBest = [];
      stopSpinner();
      console.log($scope.indexed +" é o meu pos")
      osCara = $scope.listCerimonia[positron].padrinhos;
      angular.forEach(osCara, function(shops, key){
          $scope.showTheBest[key] = angular.copy(shops);
          console.log($scope.showTheBest)
      });

    };

    // abre o modal da listagem
    $scope.bringTheMen = function(item){
      Utils.spinnerBest();
      $scope.indexed = $scope.listCerimonia.indexOf(item);
      $scope.plusBest = false;
      $scope.viewBest($scope.indexed);
      $scope.openModal(3);
      console.log("traga aqui");
    }


  $scope.moveItem = function(item, fromIndex, toIndex) {
    $scope.showTheBest.splice(fromIndex, 1);
    $scope.showTheBest.splice(toIndex, 0, item);
    $scope.liner();
  };


  $scope.defineLados = function(lado){
    console.log(lado)
    if(lado == "noivo"){
      $scope.casale.choice = "noivo";
    }
    if(lado == "noiva"){
      $scope.casale.choice = "noiva";
    }
  }

    $scope.dialNumber = function(number) {
      window.open('tel:' + number, '_system');
    }  


   function stopSpinner(){
    $timeout(function(){
      Utils.hide();
    }, 1200);
   } 


  ///////////////////////////////////////////////////////////////////////////////////
//                          Parafernalha das musicas                           //    
///////////////////////////////////////////////////////////////////////////////////
//    $scope.bestSong = false;
//    $scope.justSong = false; 
//    $scope.plusSong = false;
//    $scope.parade = {};
//    $scope.paradeIndex = 0;
//    $scope.allTheSongs= {};
//    $scope.showTheSongs = [];


   //insere o padrinho - step 1
   $scope.putSong = function(){
      $scope.plusSong = false;
      if(!$scope.parade.title || !$scope.parade.autor){
        Utils.alertshow("Erro", "Cadastro da música incompleto")
      }else{
         console.log($scope.parade);
         $scope.cadastraSong();     
      }
   };

    //insere o padrinho - step 2
    $scope.cadastraSong = function(){
        $scope.showTheSongs.push($scope.parade);
        $scope.linerSong();
   }; 

   //insere o padrinho - step 3 | //deleta o padrinho - step 2
   $scope.linerSong = function(){
        var bester = {};
        angular.forEach($scope.showTheSongs, function(shopes, keyo){
            bester[keyo] = $scope.showTheSongs[keyo];
            bester = angular.copy(bester);
            console.log(bester);
        });
        $scope.listCerimonia[$scope.indexed].playlist = bester;
        $scope.insertIntoObj();    
   }

   //deleta o padrinho - step 1
   $scope.notSoSong = function(theGuy){
        var num = 0;
        var arr = $scope.listCerimonia;
        for(var i=0; i<arr.length; i++) {
            if(arr[i].playlist) {
                // achou!
                num = i
            }
        }

        $scope.indexed = num;

        var posit = $scope.showTheSongs.indexOf(theGuy);
        $scope.showTheSongs.splice(posit, 1)       
        $scope.linerSong();
   }

   //botão para mostrar o form de inserção do padrinho
   $scope.insertSong = function(){
      $scope.plusSong = true;
      $scope.parade = {};
   };



   $scope.improveTheSong = function(ictio){

    $scope.paradeIndex = $scope.showTheSongs.indexOf(ictio);
    $scope.parade = {};
    $scope.parade = ictio;
    $scope.openModal(6);
  }

   $scope.correctSong = function(){
      
      var num = 0;
      var arr = $scope.listCerimonia;
      for(var i=0; i<arr.length; i++) {
          if(arr[i].playlist) {
              // achou!
              num = i
          }
      }
      $scope.indexed = num;

      $scope.showTheSongs[$scope.paradeIndex] = $scope.parade;
      $scope.linerSong();
      $scope.closeModal(6);
   };

    //preenche o array de playlist
    $scope.viewSong = function(positron){
      var osCara = {};
      $scope.showTheSongs = [];
      console.log($scope.indexed +" é o meu pos")
      osCara = $scope.listCerimonia[positron].playlist;
      angular.forEach(osCara, function(shops, key){
          $scope.showTheSongs[key] = angular.copy(shops);
          console.log($scope.showTheSongs)
      });

    };

    // abre o modal da listagem
    $scope.bringTheSong = function(item){
      $scope.indexed = $scope.listCerimonia.indexOf(item)
      $scope.viewSong($scope.indexed);
      $scope.openModal(5);
      console.log("traga aqui");
    }


  $scope.moveSong = function(item, fromIndex, toIndex) {
    $scope.showTheSongs.splice(fromIndex, 1);
    $scope.showTheSongs.splice(toIndex, 0, item);
    $scope.linerSong();
  };


  $scope.medetempo = function(hiato){
    var tempoFaltante = '';
    if(hiato == 0.25){
      tempoFaltante = "uma semana antes";
    }
    else if(hiato == 0.5){
     tempoFaltante = "duas semanas antes"; 
    }
    else if(hiato == 0.75){
     tempoFaltante = "três semanas antes"; 
    }
    else if(hiato == 0){
     tempoFaltante = "não definido"; 
    }
    else if(hiato == 1){
     tempoFaltante = "um mês antes"; 
    }
    else{
     tempoFaltante = hiato+"meses antes"; 
    }

    return tempoFaltante;
  }


    var preWed = {
      0:{
        item: 'Local da Cerimônia',
            time: .25,
        done: false
      },
      1:{
        item: 'Local da Festa',
            time: .25,
        done: false
      },
      2:{
        item: 'Decoração',
            time: .25,
        done: false
      },
      3:{
        item: 'Fotos',
            time: .25,
        done: false
      },
      4:{
        item: 'Vídeo',
            time: .25,
        done: false
      },
      5:{
        item: 'Música da Cerimônia',
            time: .25,
        done: false
      },
      6:{
        item: 'Música da Festa',
            time: .25,
        done: false
      },
      7:{
        item: 'Cabelo/Maquiagem',
            time: .25,
        done: false
      },
      8:{
        item: 'Vestido',
            time: .25,
        done: false
      },
      9:{
        item: 'Roupa Noivo',
            time: .25,
        done: false
      },
      10:{
        item: 'Roupa Daminha',
            time: .25,
        done: false
      },
      11:{
        item: 'Alianças',
            time: .25,
        done: false
      },
      12:{
        item: 'Buquê',
            time: .25,
        done: false
      },
      13:{
        item: 'Convite',
            time: .25,
        done: false
      },
      14:{
        item: 'Envio de Convites',
            time: .25,
        done: false
      },
      15:{
        item: 'Calígrafo',
            time: .25,
        done: false
      },
      16:{
        item: 'Casamento Civil',
            time: .25,
        done: false
      },
      17:{
        item: 'Música da Festa',
            time: .25,
        done: false
      },
      18:{
        item: 'Aluguel de Carro',
            time: .25,
        done: false
      },
      19:{
        item: 'ECAD',
            time: .25,
        done: false
      },
      20:{
        item: 'Bolo',
            time: .25,
        done: false
      },
      21:{
        item: 'Doces',
            time: .25,
        done: false
      },
      22:{
        item: 'Lembrancinhas',
            time: .25,
        done: false
      }
    };
    // Cerimônia
    var aCerimonia = {
      0:{
        item: 'Música',
            time: "18:00",
        mus: true,    
        done: false
      },
      1:{
        item: 'Entrada dos Pais',
            time: "18:00",
        done: false
      },
      2:{
        item: 'Entrada dos Noivos',
            time: "18:00",
        done: false
      },
      3:{
        item: 'Entrada dos Padrinhos',
            time: "18:00",
        bm: true,
        done: false
      },
      4:{
        item: 'Entrada da Noiva',
            time: "18:00",
        done: false
      },
      5:{
        item: 'Alianças',
            time: "18:00",
        done: false
      },
      6:{
        item: 'Cumprimentos/Assinaturas',
            time: "18:00",
        done: false
      },
      7:{
        item: 'Cortejo de Saída',
            time: "18:00",
        done: false
      }
    };

    //Festa
    var aFesta = {
      0:{
        item: 'Decoração da festa',
            time: "18:00",
        done: false
      },
      1:{
        item: 'Entrega do bolo',
            time: "18:00",
        done: false
      },
      2:{
        item: 'Entrega dos doces',
            time: "18:00",
        done: false
      },
      3:{
        item: 'DJ',
            time: "18:00",
        done: false
      },
      4:{
        item: 'Início da festa',
            time: "18:00",
        done: false
      },
      5:{
        item: 'Fotos protocolares',
            time: "18:00",
        done: false
      },
      6:{
        item: 'Corte do bolo',
            time: "18:00",
        done: false
      },
      7:{
        item: '1ª dança',
            time: "18:00",
        done: false
      },
      8:{
        item: 'Atração',
            time: "18:00",
        done: false
      },
      9:{
        item: 'Jantar',
            time: "18:00",
        done: false
      },
      10:{
        item: 'Encerramento da festa',
            time: "18:00",
        done: false
      },
      11:{
        item: 'Desmontagem',
            time: "18:00",
        done: false
      },
    };

    var antecipacao = [
      {tempo: 0, label:"não definido"},
      {tempo: .25, label:"1 semana antes"},
      {tempo: .50, label:"2 semanas antes"},
      {tempo: .75, label:"3 semanas antes"},   
      {tempo: 1, label:"1 mês antes"},
      {tempo: 2, label:"2 meses antes"},
      {tempo: 3, label:"3 meses antes"},
      {tempo: 4, label:"4 meses antes"},
      {tempo: 5, label:"5 meses antes"},
      {tempo: 6, label:"6 meses antes"},
      {tempo: 7, label:"7 meses antes"},
      {tempo: 8, label:"8 meses antes"},
      {tempo: 9, label:"9 meses antes"},
      {tempo: 10, label:"10 meses antes"},
      {tempo: 11, label:"11 meses antes"},
      {tempo: 12, label:"12 meses antes"},
      {tempo: 13, label:"13 meses antes"},
      {tempo: 14, label:"14 meses antes"},
      {tempo: 15, label:"15 meses antes"},
      {tempo: 16, label:"16 meses antes"},
      {tempo: 17, label:"17 meses antes"},
      {tempo: 18, label:"18 meses antes"}
    ];

      //$scope.timeBefore = extraData.getTimeEarly();
      $scope.timeBefore = antecipacao;

}; // fim da função
})(); // fim do documento
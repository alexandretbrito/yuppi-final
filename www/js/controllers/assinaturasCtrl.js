(function() {
  'use_strict';

  angular.module('APPlanner').controller('assinaturasCtrl', assinaturasCtrl);

  function assinaturasCtrl($scope, $rootScope, $localStorage, $sessionStorage, Utils, $cordovaNetwork, planFactory, accessFactory, $timeout) {


        $scope.options = {
          loop: false
        }

        $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
          console.log("Slider initialized: ", data);
          $scope.slider = data.slider;
        });

        $scope.assinaturas = [];
        const IAP = window.inAppPurchase;
        $scope.pagante = $rootScope.usuarioAtivo.pagante;
        $scope.planos = ["br.com.app.yuppi.mensal", "br.com.app.yuppi.semestral", "br.com.app.yuppi.anual"];
        $scope.badge = ["img/plano-bronze.png", "img/plano-prata.png", "img/plano-ouro.png"]
        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
        });


        $scope.$on("$ionicView.enter", function(event, data){
          console.log("aqui estou em Android");
          Utils.show()
          $timeout(function() {
            if(IAP && !$scope.pagante){
              console.log("planos ok");
             IAP.getProducts(["br.com.app.yuppi.mensal", "br.com.app.yuppi.semestral", "br.com.app.yuppi.anual"]).then(function(produtos){
               console.log("produtos aqui")
               Utils.hide()
               $scope.assinaturas = produtos;
               console.log($scope.assinaturas);
               console.log(`////////`)
               //$scope.apply()
             }).catch(function (err) {
              console.log(err);
            });
           }
         }, 3000);
        });

        $scope.buy = function(id_prod){
            /*subscribe ?*/
         IAP.subscribe(id_prod).then((data) => {
           console.log("comprado com sucesso")
           console.log(data);
           $localStorage.usuario.transactionId = data.transactionId;
           $localStorage.usuario.pagante = true;
           var novosDados2 = $localStorage.usuario;
           var dataChange2 = accessFactory.pegaUsuario($localStorage.usuario.uid);
           dataChange2.update(novosDados2).then(function(success){
               console.log("deu")
           }, function(error){
               console.log("não deu")
               console.log(error);
           })  
         });        
        }
  }; //fim da função
})(); // fim do documento
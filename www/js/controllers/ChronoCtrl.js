(function() {
'use_strict';

angular.module('APPlanner').controller('ChronoCtrl', ChronoCtrl);

function ChronoCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, Utils, $ionicPopup, $filter){

   $scope.$parent.showHeader();

    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;

    $scope.listCheck = [];

      if($rootScope.asInvited){
        if($localStorage.colabs[$scope.theEvent].chron){
            var bjec = {};
            bjec = $localStorage.colabs[$scope.theEvent].chron;
            angular.forEach(bjec, function(shops, key){

                $scope.listCheck[key] = angular.copy(shops);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)

        }else{
            console.log("segue vazio")
        }
      }else{
        if($localStorage.eventos[$scope.theEvent].chron){
            var bjec = {};
            bjec = $localStorage.eventos[$scope.theEvent].chron;
            angular.forEach(bjec, function(shops, key){

                $scope.listCheck[key] = angular.copy(shops);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)

        }else{
            console.log("segue vazio")
        }
      }

        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
         console.log("eu sou um evento genérico")
        }); 

        $scope.showTaskPrompt = function() {
            var newShop = {
                item: '',
                time: '14:00',
                done: false
            };  

              $scope.newShop = newShop;
              var chop = $scope.newShop.time;
              var arr = chop.split(':');
              var hr = arr[0];
              var mn = arr[1];
              var saveData = new Date(0);
              saveData.setHours(hr);
              saveData.setMinutes(mn);
              $scope.itemTime = {
                  value: saveData
              };
            $scope.openModal(1);
        };

        $scope.saveTask = function(indice) {
          if($scope.newShop.item != ''){
              console.log("gravou")
              var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
              $scope.newShop.time = datafinal;
              $scope.listCheck.push($scope.newShop);
              $scope.closeModal(indice);
              $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do item")
          }
        };

        $scope.cancelTask = function(indice) {
            $scope.closeModal(indice);
        };

///////////////////////////////////////////////////////////////////////////////////

$scope.eventTime = {
        callback: function (val) {      //Mandatory
            console.log ("este é "+val);
            $scope.itemTime = val;
            $scope.horaok = true
        },
        //inputTime: 50400,   //Optional
        format: 24,         //Optional
        step: 5,           //Optional
        setLabel: 'Ok'    //Optional
      };

         $scope.normHour = function(){
         ionicTimePicker.openTimePicker($scope.eventTime);
       };

        $scope.acertaZero = function(minuts){
            if(minuts !== 0){
                return minuts
            }else{
                console.log("ops");
                return "00"
            }
        };   

        $scope.horeador = function(tictac){
            var selectedTime = new Date(tictac * 1000);
            console.log('Selected epoch is : ', tictac, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
            var toe = selectedTime.getUTCHours()+":"+$scope.acertaZero(selectedTime.getUTCMinutes());
            if(toe !== '0:00' || $scope.horaok === true){
                return toe;
            }else{
                return "";
            }
        };

///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].chron = $scope.listCheck;
        }else{
          $localStorage.eventos[$scope.theEvent].chron = $scope.listCheck;
        }
        $scope.insertIntoObj();
    }

    $scope.removeMe = function(itim){
        console.log(itim.item)
        //$scope.listCheck.splice(indi, 1)
        var aquele = $scope.listCheck.indexOf(itim)
        console.log(aquele)
        $scope.listCheck.splice(aquele, 1)
        $scope.insertIntoObj();
    }

    $scope.delEvento = function(){
       // A confirm dialog
       var confirmPopup = $ionicPopup.confirm({
         title: 'Deletar',
         template: 'Tem certeza que quer deletar?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() { 
             console.log('You are sure');
           $scope.removefromUpdate(2)        
           }
         }
        ]
       })

       confirmPopup.then(function(res) {
        console.log(res)
         if(res) {

         } else {
           console.log('You are not sure');
         }
       });
    }; 

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
        $scope.listCheck.splice($scope.indiceMod, 1)
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
        if($scope.connec){
            console.log("vou gravar na internet");
            if($rootScope.asInvited){
              planFactory.insertCrono($rootScope.eventoDaVez.uid, $scope.theEvent, objeto)
            }else{
              planFactory.insertCrono($localStorage.usuario.uid, $scope.theEvent, objeto)
            }
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }   


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(shopes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].chron = bje;
        }else{
          $localStorage.eventos[$scope.theEvent].chron = bje;
        }
        //ionicToast.show('Sucesso!', 'middle', false, 3000);
        $scope.grave_me(bje);        
    };


    $scope.editTask = function(index, obito){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.modifItem = obito;
              if($scope.modifItem.time == 0){
                  var datainicial = new Date(0);
                  $scope.itemTime = {
                   value: datainicial
                  };  
              }else{
                  var chop = $scope.modifItem.time;
                  var arr = chop.split(':');
                  var hr = arr[0];
                  var mn = arr[1];
                  var datainiciada = new Date(0);
                  datainiciada.setHours(hr);
                  datainiciada.setMinutes(mn);
                  $scope.itemTime = {
                      value: datainiciada
                  };          
              }; 
        console.log("entrou")
        console.log($scope.modifItem)        
        $scope.openModal(2)
    };  

    $scope.updateTask = function(indice) {
        console.log("saiu")
        var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
        $scope.modifItem.time = datafinal;
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };

     $ionicModal.fromTemplateUrl('templates/auxiliar/inserecronograma.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/modificacronograma.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }else{
       $scope.modal2.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }else{
       $scope.modal2.hide();
      }
   
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });

}; // fim da função
})(); // fim do documento
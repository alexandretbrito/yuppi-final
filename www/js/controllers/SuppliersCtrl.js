(function() {
'use_strict';

angular.module('APPlanner').controller('SuppliersCtrl', SuppliersCtrl);

function SuppliersCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $filter, $cordovaNetwork, planFactory, Utils, $ionicPopup, $cordovaContacts, $timeout){
    $scope.$parent.showHeader();
    var permissions = cordova.plugins.permissions;
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;

    $scope.listCheck = [];

	$scope.evtPreCad = true;
 	$scope.otherEvtType = "";
	$scope.evtType = "";
  $scope.pagType = "";   
  $scope.selectIndex = 0;
  $scope.parcelas = [];
  $scope.calc_result = "";

      if($rootScope.asInvited){
        if($localStorage.colabs[$scope.theEvent].fornecedores){
            var bjec = {};
            bjec = $localStorage.colabs[$scope.theEvent].fornecedores;
            angular.forEach(bjec, function(shops, key){

                $scope.listCheck[key] = angular.copy(shops);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)

        }else{
            console.log("segue vazio")
        }
      }else{     
        if($localStorage.eventos[$scope.theEvent].fornecedores){
            var bjec = {};
            bjec = $localStorage.eventos[$scope.theEvent].fornecedores;
            angular.forEach(bjec, function(shops, key){

                $scope.listCheck[key] = angular.copy(shops);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)

        }else{
            console.log("segue vazio")
        }
      }

        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
         prepare_perm();
        }); 

        function prepare_perm() {
          // body...
            permissions.checkPermission(permissions.READ_CONTACTS, function( status ){
            if ( status.hasPermission ) {
              console.log("Yes :D ");
            }
            else {
              console.log("No :( ");
              permissions.requestPermission(permissions.READ_CONTACTS, success, error);
               
              function error() {
                console.log('Contacts permission is not turned on');
              }
               
              function success( status ) {
                if( !status.hasPermission ) error();
              }          
            }
             });
        };

        $scope.showTaskPrompt = function() {
            var newShop = {
                nome: '',
                tel: '',
                email: '', 
                tipo: '',
                valor_contrato:'',
                forma_pagto:'',
                obs:''
            };  

            $scope.newShop = newShop;
            $scope.openModal(1);
        };

        $scope.saveTask = function(indice) {
         if($scope.newShop.nome == "" || $scope.evtType == "Tipo de Fornecedor" || $scope.evtType == ""  || $scope.evtType == ""){
            Utils.alertshow("Erro", "Nome, telefone ou tipo de fornecedor não foram preenchidos. Verifique e tente novamente.")
         }else{
      			if($scope.evtType !== "Outro"){
      				$scope.newShop.tipo = $scope.evtType;
      			}
            if($scope.evtType === "Tipo de Fornecedor"){
              $scope.evtType = '';
              console.log("Tipo de Fornecedor");   
            }
            if( $scope.parcelas.length > 0 ){
              console.log($scope.parcelas)
              //venc  //valor
              //tempo
              var tempParc = []
              var quant = $scope.parcelas.length;
              angular.forEach($scope.parcelas, function(hori, chuvs){
                var quid = hori.venc;
                hori.venc = String($scope.convertDate(quid));
                hori = angular.copy(hori);
                tempParc.push(hori)
                if(chuvs+1 == quant){
                  $scope.newShop.parcelamento = tempParc;
                }
              })
            }

            $scope.newShop.forma_pagto = $scope.pagType;
            $scope.newShop.tipo = $scope.evtType;
            console.log("gravou")
            $scope.listCheck.push($scope.newShop);
            $scope.closeModal(indice);
            $scope.insertIntoObj();
        };   
     };

        $scope.cancelTask = function(indice) {
            $scope.closeModal(indice);
            $scope.cleanSupliers();
        };


        $scope.cleanSupliers = function(){
            $scope.pagType = "";
            $scope.evtType ="";
            $scope.mySelect.tips = "Tipo de Fornecedor";
            $scope.pagType = "Qual a forma de pagamento?";
            $scope.parcelas = [];
            $scope.formaPagto.forma = "Qual a forma de pagamento?";
        }

///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].fornecedores = $scope.listCheck;
        }else{
          $localStorage.eventos[$scope.theEvent].fornecedores = $scope.listCheck;
        }
        $scope.insertIntoObj();
    }

    $scope.removeMe = function(itim){
        console.log(itim.item)
        //$scope.listCheck.splice(indi, 1)
        var aquele = $scope.listCheck.indexOf(itim)
        console.log(aquele)
        $scope.listCheck.splice(aquele, 1)
        $scope.insertIntoObj();
    }    

  $scope.delEvento = function(){
     // A confirm dialog
     var confirmPopup = $ionicPopup.confirm({
       title: 'Deletar',
       template: 'Tem certeza que quer deletar?',
       cssClass: 'my-popup',
       buttons: [
         { text: 'Não',
            type: 'button-assertive myred',
          },
       {
         text: 'Sim',
         type: 'button-positive-900',
         onTap: function() { 
           console.log('You are sure');
         $scope.removefromUpdate(3)        
         }
       }
      ]
     })

     confirmPopup.then(function(res) {
      console.log(res)
       if(res) {

       } else {
         console.log('You are not sure');
       }
     });
  };

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
        $scope.listCheck.splice($scope.indiceMod, 1)
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
        if($scope.connec){
            console.log("vou gravar na internet");
            if($rootScope.asInvited){
              planFactory.insertSup($rootScope.eventoDaVez.uid, $scope.theEvent, objeto)
              $scope.clearAll();
            }else{
              planFactory.insertSup($localStorage.usuario.uid, $scope.theEvent, objeto)
              $scope.clearAll();
            }
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
            $scope.clearAll();
        }
    }   

    $scope.clearAll = function(){
      $timeout(function(){
        $scope.evtPreCad = true;
        $scope.formaPagto.forma = "Qual a forma de pagamento?";
        $scope.otherEvtType = "";
        $scope.evtType = "";
        $scope.pagType = "";
      }, 800);       
    }



    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(shopes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].fornecedores = bje;
        }else{
          $localStorage.eventos[$scope.theEvent].fornecedores = bje;
          console.log("gravou")
        }
        //ionicToast.show('Sucesso!', 'middle', false, 3000);
        $scope.grave_me(bje);        
    };

    $scope.showTask = function(index, obito){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.modifItem = obito;
        console.log("entrou")
        console.log($scope.modifItem)
        $scope.openModal(2)
    };  

    $scope.editTask = function(index, obito){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        console.log("edit task");
        console.log($scope.pagType);
          $scope.parcelas = [];
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.modifItem = obito;
        $scope.mySelect.tips = "Tipo de Fornecedor";
        $scope.pagType = "Qual a forma de pagamento?";
        if($scope.modifItem.parcelamento){
          angular.forEach($scope.modifItem.parcelamento, function(valet, kay){
            
            var datinha = "";
            if(valet.venc){
              console.log("tem data em venc")
              var dataTrans = new Date(valet.venc);
              console.log(dataTrans)
              valet.venc = dataTrans;

            }else{
              console.log("não tem data em venc")
            }
            $scope.parcelas.push(valet);
          });
        }
        angular.forEach($scope.tipolog, function(shops, key){
            if(shops.tips == $scope.modifItem.tipo){
               console.log("Yeeeei "+key)
               $scope.selectIndex = key;
               $scope.chosenOne = shops.tips
            }    
        });
        $scope.openModal(3)
        console.log("entrou")
        if($scope.modifItem.forma_pagto == "Nenhuma" || $scope.modifItem.forma_pagto == undefined || $scope.modifItem.forma_pagto == ""){
          console.log("Não se aplica a primeira");
          $scope.formaPagto.forma = $scope.pagType = "Qual a forma de pagamento?"
        }else{
          console.log("Não se aplica a segunda");
          $scope.formaPagto.forma = $scope.pagType = $scope.modifItem.forma_pagto;
        }

        $scope.mySelect.tips = $scope.chosenOne;
        console.log($scope.mySelect.tips +" após");
        console.log($scope.tipolog[$scope.selectIndex].tips +" só pra conferir");
        console.log($scope.tipolog[0].tips +" é o que tem de ser se não tiver nada");
        if($scope.modifItem.tipo == "" || $scope.modifItem.tipo == undefined){
          $scope.tipolog[0].tips = "Tipo de Fornecedor" 
          $scope.mySelect.tips = "Tipo de Fornecedor";
        }
        console.log($scope.formaPagto.forma +" é o pagt");
        console.log($scope.modifItem)        

        //$scope.preterChoose(); 
    };  

    $scope.updateTask = function(indice) {
        console.log("saiu")
        console.log($scope.modifItem);
        $scope.modifItem.forma_pagto = $scope.pagType;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        if($scope.chosenOne !== $scope.evtType){
          $scope.modifItem.tipo = $scope.evtType;
        }
            if( $scope.parcelas.length > 0 ){
              console.log($scope.parcelas)
              //venc  //valor
              //tempo
              var tempParc = []
              var quant = $scope.parcelas.length;
              angular.forEach($scope.parcelas, function(hori, chuvs){
                var quid = hori.venc;
                hori.venc = String($scope.convertDate(quid));
                hori = angular.copy(hori);
                tempParc.push(hori)
                if(chuvs+1 == quant){
                  $scope.modifItem.parcelamento = tempParc;
                }
              })
            }
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };

     $ionicModal.fromTemplateUrl('templates/auxiliar/inseresupplier.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/viewsupplier.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

     $ionicModal.fromTemplateUrl('templates/auxiliar/editasupplier.html', {
      id: '3',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal3 = modal;
   });

     $ionicModal.fromTemplateUrl('templates/auxiliar/capturasupplier.html', {
      id: '4',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal4 = modal;
   });     

    
   $scope.openModal = function(index) {
      if (index == 1){
        $scope.modal1.show();
      }else if (index == 2){
        $scope.modal2.show();
      }else if (index == 3){
        $scope.modal3.show();
      } else{
        $scope.modal4.show();
      }
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
        $scope.modal1.hide();
      }else if (index == 2){
        $scope.modal2.hide();
      }else if (index == 3){
        $scope.modal3.hide();
      }
      else{
        $scope.modal4.hide();
        console.log($scope.search.displayName)
        $scope.search = {};
      }
  };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
      $scope.modal3.remove();
      $scope.modal4.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });   

  $scope.pagueMe = function(pg){
    $scope.pagType = pg.forma;
    console.log($scope.pagType);
    if($scope.pagType == "Qual a forma de pagamento?"){
      $scope.pagType = "Nenhuma";
    }
  }

 $scope.changeEventType = function(sel){
      console.log(sel);
  		$scope.evtType = sel.tips;
      console.log($scope.evtType); 
  	if($scope.evtType === "Outro"){
  		$scope.evtPreCad = false;
      console.log("não está na lista");		
  	}
    if($scope.evtType !== "Outro"){
        console.log("está na lista");
       $scope.evtPreCad = true;     
    }else{
      $scope.evtPreCad = false;
      console.log("não está na lista");   
    }
  };



  $scope.preterChoose = function(){
      console.log("preterChoose")
      var dito = $scope.modifItem.tipo;
      console.log(dito)
      angular.forEach($scope.tipolog, function(shops, key){
          if(shops.tips == dito){
             console.log("Yeeeei "+key)
             $scope.mySelect = $scope.tipolog[key].tips;
          }    
      });
      if(zz != 0){
        $scope.evtPreCad = true;
      }else{
        $scope.evtPreCad = false;
      }
  };

  $scope.pagform = [
    {forma:"Qual a forma de pagamento?"},
    {forma:"Dinheiro"},
    {forma:"Cheque"},
    {forma:"Cartão de crédito"},
    {forma:"Boleto bancário"},
    {forma:"Depósito/transferência"},
    {forma:"DOC/TED"}
  ]

  $scope.tipolog = [
    {tips: "Tipo de Fornecedor"},
    {tips: "Bolo"},
    {tips: "Doce"},
    {tips: "Bebidas"},
    {tips: "Convite"},
    {tips: "Móveis"},
    {tips: "Itens de Decoração"},
    {tips: "Iluminação"},
    {tips: "DJ"},
    {tips: "Foto"},
    {tips: "Filme"},
    {tips: "Lembranças"},
    {tips: "Buffet"},
    {tips: "Animação"},
    {tips: "Atração Musical"},
    {tips: "Vestido"},
    {tips: "Flores"},
    {tips: "Jóias"},
    {tips: "Celebrante"},
    {tips: "Cabelo/Maquiagem"},
    {tips: "Segurança"},
    {tips: "Manobrista"},
    {tips: "Bem Casado"},
    {tips: "Chocolate"},
    {tips: "Limpeza"},
    {tips: "Staff"},
    {tips: "Coreógrafo"},
    {tips: "Decorador"},
    {tips: "Local"},
    {tips: "Transporte"},
    {tips: "Brindes"},
    {tips: "Outro"}
  ]


  $scope.mySelect = $scope.tipolog[0];

  $scope.formaPagto = $scope.pagform[0];

    $scope.dialNumber = function(number) {
      window.open('tel:' + number, '_system');
    }


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
  // capturar contatos do telefone

  $scope.captaContact = function(){

    $scope.openModal(4);
    Utils.show();
    $cordovaContacts.find([]).then(function(results){
        Utils.hide();
        $scope.contatos = results;
        console.log($scope.contatos)
    }, function(error){
        Utils.hide();
        console.log(error);
        Utils.alertshow("Erro", "Não foi possível carregar sua lista de contatos");
        $scope.closeModal(4);
    });
  };

  $scope.chooseMyContact = function(contic){
      $scope.closeModal(4);
      console.log($scope.mySelect)
      $scope.mySelect = $scope.mySelect = $scope.tipolog[0].tips;
      console.log(contic)
      console.log($scope.mySelect)
      if(contic.name.formatted != undefined){
        var nominic = contic.name.formatted;
      }else{
        var nominic = "";
      }
      if(contic.phoneNumbers != undefined){
        var telonius = contic.phoneNumbers[0].value;
      }else{
        var telonius = "";
      }


      var newShop = {
          nome: nominic,
          tel: telonius,
          email: '', 
          tipo: '',
          valor_contrato:'',
          forma_pagto:'',
          obs:''
      };
       $scope.newShop = newShop;
       $scope.openModal(1);
  };

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
/* Parcelas */

  
  $scope.addNewChoice = function() {
    var agora = new Date()
    var newItemNo = $scope.parcelas.length+1;
    $scope.parcelas.push({venc:agora, parcela:"0,00", pg: false});
  };
    
  $scope.removeChoice = function() {
    var lastItem = $scope.parcelas.length-1;
    $scope.parcelas.splice(lastItem);
  };



$scope.convertDate = function(date){
    var myDate = new Date(date);
        var month = lessThanTen(myDate.getMonth() + 1);
        var date = lessThanTen(myDate.getDate());
        var year = myDate.getFullYear();
        var format = year + '-' + month + '-' + date
        return format;
  }
  function lessThanTen(value) {
    return value < 10 ? '0' + value : value;
  } 

$scope.bringDate = function(when){
  var diaD = when;
  var diaArr = [];
  console.log()
  diaArr = diaD.toString().split('/');
  var finalCountDown = new Date(diaArr[2], diaArr[1], diaArr[0]);
//  var finalDate = new Date(finalCountDown)

    return finalCountDown;
}




}; // fim da função
})(); // fim do documento